package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import assemble.Model.ActionLogEntry;

public class JPlayerGui extends JFrame {
    /**
     *
     */
    private static final long serialVersionUID = -5918986892583977471L;
    private JTextArea display;
    private JTextArea message;

    public JPlayerGui(String name) {
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout(5, 5));
        display = new JTextArea();
        message = new JTextArea();
        display.setFont(new Font("monospaced", Font.PLAIN, 10));
        message.setFont(new Font("monospaced", Font.PLAIN, 10));
        JSplitPane splitPane = new JSplitPane(
            JSplitPane.VERTICAL_SPLIT,
            new JScrollPane(display),
            new JScrollPane(message)
        );
        splitPane.setDividerLocation(250);
        contentPane.add(splitPane);
        this.setTitle("Player: " + name);
        this.setContentPane(contentPane);
        this.pack();
    }

    public void display() {
        this.setLocationByPlatform(true);
        this.setSize(400,500);
        this.setVisible(true);
    }

    public void updateDisplay(String state, List<ActionLogEntry> actionLog) {
        display.setText(state);

        StringBuilder buf = new StringBuilder();
        for (ActionLogEntry entry : actionLog) {
            buf.append(entry.toString()).append("\n");
        }
        message.setText(buf.toString());
    }
}