package assemble;

import com.fasterxml.jackson.databind.JsonNode;

import graph.util.Position;

public class Thing {
    public Position<Thing> position;
    public int x;
    public int y;
    public String details;
    public String type;          // entity, block, dispenser, marker

    public Thing(Model model, JsonNode node) {         // create Thing with absolute coords relative to agents starting position
        x = model.abs_x(node.get("x").asInt());
        y = model.abs_y(node.get("y").asInt());
        details = node.get("details").asText();
        type = node.get("type").asText();
    }
    
    public Thing(JsonNode node) {                  // create Thing with relative coords
        x = node.get("x").asInt();
        y = node.get("y").asInt();
        details = node.get("details").asText();
        type = node.get("type").asText();
    }
    
    @Override
    public boolean equals(Object object) {
        if (!Thing.class.isInstance(object)) return false;
        Thing thing = (Thing) object;
        return (x == thing.x) && (y == thing.y) && details.equals(thing.details) && type.equals(thing.type);
    }

    public String toString() {
        return type + "(" + details + "," + x + "," + y + ")";
    }
}