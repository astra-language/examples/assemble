package assemble;

import com.fasterxml.jackson.databind.JsonNode;

import core.MessageHandler;

public class SimStartHandler implements MessageHandler {
    private Model model;

    public SimStartHandler(Model model) {
        this.model = model;
    }

    @Override
    public void handle(JsonNode message) {
        System.out.println("sim-start: " + message);

        JsonNode percept = message.get("content").get("percept");
        
        model.teamSize = percept.get("teamSize").asInt();
        model.team = percept.get("team").asText();
        model.name = percept.get("name").asText();
        model.vision = percept.get("vision").asInt();
        model.steps = percept.get("steps").asInt();
    }
    
}