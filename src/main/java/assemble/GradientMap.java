package assemble;

import java.util.Map;
import java.util.TreeMap;

import graph.util.Heap;
import graph.util.Position;

public class GradientMap {
    private static final int[][] OPTIONS = {new int[] {0, -1}, new int[] {1, 0}, new int[] {0,1}, new int[] {-1,0}};
    private static final String[] DIRECTIONS = { "n", "e", "s", "w"};
    public class Entry {
        int x;
        int y;
        int distance;

        public Entry(int x, int y, int distance) {
            this.x = x;
            this.y = y;
            this.distance = distance;
        }

        public int priority() {
            // int px = x-model.x;
            // int py = y-model.y;

            // return (int) Math.sqrt((double) px*px+py*py) - distance;
            return distance;
        }
    }

    Model model;
    int[][] map;

    public GradientMap(Model model) {
        this.model = model;
    }

    private int grid_x(int x) {
        return x+GridMap.SIZE/2;
    }

    private int grid_y(int y) {
        return y+GridMap.SIZE/2;
    }

    public void generateGradient(int x, int y) {
        map = new int[GridMap.SIZE][GridMap.SIZE];

        Heap<Integer, Entry> heap = new Heap<>();
        Map<String, Position<Entry>> positionMap = new TreeMap<>();
        
        Entry entry = new Entry(x, y, 1);
        String key = key(x, y);
        positionMap.put(key, heap.insert(entry.priority(), entry));
        while (!heap.isEmpty()) {
            entry = heap.remove();

            if (isClear(entry.x, entry.y)) {
                map[grid_x(entry.x)][grid_y(entry.y)] = entry.distance;

                for (int[] coord : OPTIONS) {
                    int cx = entry.x+coord[0];
                    int cy = entry.y+coord[1];
    
                    if (model.inBounds(cx, cy)) {
                        String k = key(cx, cy);
                        if (positionMap.containsKey(k)) {
                            Position<Entry> position = positionMap.get(k);
                            if (position.element().distance > entry.distance+1) {
                                //better path has been found...
                                position.element().distance = entry.distance+1;
                                heap.replaceKey(position, entry.distance+1);
                            }
                        } else {
                            Entry newEntry = new Entry(cx, cy, entry.distance+1);
                            positionMap.put(k, heap.insert(newEntry.priority(), newEntry));
                        }
                    }
                }
            } else {
                map[grid_x(entry.x)][grid_y(entry.y)] = 999;
            }

        }
    }

    private String key(int x, int y) {
        return x + ":" + y;
    }
    
    private boolean isClear(int x, int y) {
        return map[grid_x(x)][grid_y(y)] == 0 && model.getMap().isFree(x, y);
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();
        for (int j=model.min_y; j<=model.max_y;j++) {
            if (j==model.min_y) {
                buf.append("      ");
                for (int i=model.min_x; i<=model.max_x;i++) {
                    buf.append(" | ");
                    padLabel(buf, i);
                }
                buf.append(" |\n");
            }

            padLabel(buf, j);
            buf.append(" ");
            for (int i=model.min_x; i<=model.max_x;i++) {
                buf.append(" | ");
                padLabel(buf, map[grid_x(i)][grid_y(j)]);
            }
            buf.append(" |\n");
        }
        return buf.toString();
    }

    private void padLabel(StringBuilder buf, int label) {
        if (label < 0) {
            if (label > -1000) buf.append(" ");
            if (label > -100) buf.append(" ");
            if (label > -10) buf.append(" ");
            
        } else {
            buf.append(" ");
            if (label < 1000) buf.append(" ");
            if (label < 100) buf.append(" ");
            if (label < 10) buf.append(" ");
        }
        buf.append(label);
    }

    private int getDistance(int x, int y) {
        return map[grid_x(x)][grid_y(y)];
    }

    public int getDistance(int[] coords) {
        return map[grid_x(coords[0])][grid_y(coords[1])];
    }

    public int chooseOption(int[] coords) {
        int max = getDistance(coords);

        int option = -1;
        for (int i=0; i<OPTIONS.length; i++) {
            int distance = getDistance(coords[0]+OPTIONS[i][0], coords[1]+OPTIONS[i][1]);
            if (distance < max) {
                max = distance;
                option = i;
            }
        }
        return option;
    }

    public int[] optionCoords(int[] coords, int option) {
        coords[0] += OPTIONS[option][0];
        coords[1] += OPTIONS[option][1];
        return coords;
    }

    public String optionDirection(int option) {
        return DIRECTIONS[option];
    }
}