package assemble;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.cfg.ConfigOverrides;

import core.Action;
import affordances.Detach;
import affordances.Request;
import affordances.Rotate;
import affordances.Affordance;
import affordances.Attach;
import affordances.Move;
import assemble.Coordinates;
import affordances.Submit;
import affordances.Accept;
import assemble.Coordinates;
import affordances.Connect;
import affordances.Disconnect;
import affordances.Clear;

import java.lang.Math.*;

public class Model {
    public static List<AffordanceGenerator> generators = new LinkedList<>();

    static {
        generators.add(new MoveGenerator());
    }
    
    public static class ActionLogEntry {
        int step;
        Action action;

        public ActionLogEntry(int step, Action action) {
            this.step = step;
            this.action = action;
        }

        public String toString() {
            return step + ". " + action.toString();
        }
    }

    public static final String CLEAR = "clear";
    public static final String ENTITY = "entity";
    public static final String BLOCK = "block";

    private static Map<String, String> terrainViewMap = new TreeMap<>();
    private static Map<String, String> thingsViewMap = new TreeMap<>();

    public List<Action> affordances = new LinkedList<>();

    static {
        thingsViewMap.put("taskboard", "T");
        thingsViewMap.put("marker", "M");
        thingsViewMap.put("dispenser", "D");
        
        terrainViewMap.put(CLEAR, " ");
        terrainViewMap.put("goal", "G");
        terrainViewMap.put("obstacle", "O");
    }

    // Model listener
    List<ModelListener> listeners = new LinkedList<>();

    // Map Settings
    public GridMap map = new GridMap();
    public Map<String, List<Thing>> things = new TreeMap<>();   //String=type of thing, list=list of things of that type
   

    public int min_x = 0;
    public int min_y = 0;
    public int max_x = 0;
    public int max_y = 0;

    public int x;   // absolute coordinates
    public int y;   // absolute coordinates



    // sim-start values
    public String name;
    public String team;
    public int teamSize;
    public int vision;
    public int steps;

    // request-action values
    public int actionId;
    public int step;

    // Last Action
    public Action lastAction;
    public List<ActionLogEntry> actionLog = new LinkedList<>();

    // Last message
    public JsonNode message;

    public Map<String, Player> players = new TreeMap<>();
    public List<Thing> teammates = new LinkedList<>();
    public List<Thing> movedTeammates = new LinkedList<>();

    /**
     * START
     * Barry's code
     */
    
    /**
     * Update current step
     */
    public void updateStep(int step){
        this.step = step;


    }

    public Map<String,Thing> attachments = new TreeMap<>();              // relative coordinates (to the agent)
    public Task task;            // Agent's current task
    int clearSteps;               // extracted from simulation_config file
    public Map<String,Thing> taskboards = new TreeMap<>();
    public Map<String, Task> taskMap = new TreeMap<>();
    

    
    // ********************************************************************************
    // Action Model
    // ******************************************************************************

    public static final String SKIP = "skip";

    public static final String MOVE = "move";        // direction
    public static final String ATTACH = "attach";    // direction
    public static final String DETACH = "detach";    // direction
    public static final String REQUEST = "request";      // direction 

    public static final String ROTATE = "rotate";    // direction (CW or CCW)

    public static final String CONNECT = "connect";  // agent, x, y
    public static final String DISCONNECT = "disconnect";  // attachment1(x,y), attachment2(x,y)
    public static final String CLEAR_AREA = "clear";   // target (x,y)
   
    public static final String SUBMIT = "submit";    // taskName
    public static final String ACCEPT = "accept";      // taskName

    public void updateTaskboards(List<Thing> things){
        for (Thing thing : things) {
            if(thing.type.equals("taskboard")){
                if(!taskboards.containsKey(thing.x+":"+thing.y)){
                    String x_val = String.valueOf(thing.x+x);
                    String y_val = String.valueOf(thing.y+y);
                    taskboards.put(x_val+":"+y_val, thing);
                }
            }
        }
    }
     
    // public void validActions(){ 
    //     move();
    // }

    // public List<Move> move(){
    //     List<Move> moves = new LinkedList<>(); 
    //         List<Thing> things = map.getThings(x, y+1);
            
    //         if(map.isFree(x,y-1)){
    //             Move move = new Move();
    //             move.direction = "n";
    //             moves.add(move);
    //         }
    //         if(map.isFree(x,y+1)){
    //             Move move = new Move();
    //             move.direction = "s";
    //             moves.add(move);
    //         }
    //         if(map.isFree(x+1,y)){
    //             Move move = new Move();
    //             move.direction = "e";
    //             moves.add(move);
    //         }
    //         if(map.isFree(x-1,y)){
    //             Move move = new Move();
    //             move.direction = "w";
    //             moves.add(move);
    //         }
    //        return moves;
        
    // }

    public void updateAttachments(List<Thing> things, String direction){
       
        if(direction.equals("n")){
            for(Thing thing : things){ 
                if(thing.x == 0 && thing.y == -1 && thing.type.equals("block")){
                    System.out.println("\nTESTING UpdateAttachments method North: "+String.valueOf(thing.x)+":"+String.valueOf(thing.y));
                    attachments.put(String.valueOf(thing.x)+":"+String.valueOf(thing.y),thing);
                }
            }
        }

        if(direction.equals("s")){
            for(Thing thing : things){
                
                if(thing.x == 0 && thing.y == 1 && thing.type.equals("block")){
                    System.out.println("\nTESTING UpdateAttachments method South: "+String.valueOf(thing.x)+":"+String.valueOf(thing.y));
                    attachments.put(String.valueOf(thing.x)+":"+String.valueOf(thing.y),thing);
                }
            }
        }

        if(direction.equals("e")){
            for(Thing thing : things){
                
                if(thing.x == 1 && thing.y == 0 && thing.type.equals("block")){
                    System.out.println("\nTESTING UpdateAttachments method EAST: "+String.valueOf(thing.x)+":"+String.valueOf(thing.y));
                    attachments.put(String.valueOf(thing.x)+":"+String.valueOf(thing.y),thing);
                }
            }
        }

        if(direction.equals("w")){
            for(Thing thing : things){
                if(thing.x == -1 && thing.y == 0 && thing.type.equals("block")){
                    System.out.println("\nTESTING UpdateAttachments method WEST: "+String.valueOf(thing.x)+":"+String.valueOf(thing.y));
                    attachments.put(String.valueOf(thing.x)+":"+String.valueOf(thing.y),thing);
                }
            }
        }

        //TESTING print statement
        for (String name: attachments.keySet()){
            String value = attachments.get(name).toString();  
            System.out.println(name + " " + value);  
        } 

    }

    public void deleteAttachment(String direction){
            
        if(direction.equals("n")){
            if(attachments.containsKey("0:-1")){
                attachments.remove("0:-1");
            }
        }

        if(direction.equals("s")){ 
            if(attachments.containsKey("0:1")){
                attachments.remove("0:1");
            }
        }

        if(direction.equals("e")){   
            if(attachments.containsKey("1:0")){
                attachments.remove("1:0");
            }
        }

        if(direction.equals("w")){    
            if(attachments.containsKey("-1:0")){
                attachments.remove("-1:0");
            }
        }
    }

    public List<Attach> attach(List<Thing> things, JsonNode attached){
       
        List<Attach> attaches = new LinkedList<>();
        Attach attach = new Attach(); 
        ListIterator<Thing> iterator = things.listIterator(0);
        boolean isNotAttachment = false;
        while(iterator.hasNext()){
            Thing thing = iterator.next();
            if(thing.type.equals("entity") && thing.details.equals("B")){
                iterator.remove();
                isNotAttachment = true;
            }
            if(thing.type.equals("dispenser")){
                iterator.remove();
                isNotAttachment = true;
            }
            if(isNotAttachment){
                for(JsonNode j : attached){
                    if( (j.get(0).asInt() == thing.x) && j.get(1).asInt() == thing.y){
                        iterator.remove();                          
                    }
                }    
            }
        }

        for(Thing thing : things){  
            if ((thing.x == 0 && thing.y == -1)){
                attach.attachType = thing.type;
                attach.details = thing.details;
                attach.direction = "n";
                attaches.add(attach);
            }
            if (thing.x == 0 && thing.y == 1){
                attach.attachType = thing.type;
                attach.details = thing.details;
                attach.direction = "s";
                attaches.add(attach);
            }
            if (thing.x == 1 && thing.y == 0){
                attach.attachType = thing.type;
                attach.details = thing.details;
                attach.direction = "e";
                attaches.add(attach);
            }
            if (thing.x == -1 && thing.y == 0){
                attach.attachType = thing.type;
                attach.details = thing.details;
                attach.direction = "w";
                attaches.add(attach);
            }                    
        }  
        return attaches;
    }

    public List<Detach> detach(){

        List<Detach> detaches = new LinkedList<>();
        if(attachments.containsKey("0:-1") == true){
            Detach detach = new Detach();
            Thing thing = attachments.get("0:-1");
            detach.direction = "n";
            detach.type = thing.type;
            detaches.add(detach);
        } 
        if(attachments.containsKey("0:1") == true){
            Detach detach = new Detach();
            Thing thing = attachments.get("0:1");
            detach.direction = "s";
            detach.type = thing.type;
            detaches.add(detach);
        }   
        if(attachments.containsKey("1:0") == true){
            Detach detach = new Detach();
            Thing thing = attachments.get("1:0");
            detach.direction = "e";
            detach.type = thing.type;
            detaches.add(detach);
        }  
        if(attachments.containsKey("-1:0") == true){
            Detach detach = new Detach();
            Thing thing = attachments.get("-1:0");
            detach.direction = "w";
            detach.type = thing.type;
            detaches.add(detach);
        } 
         return detaches;
    }

    // public void updateAttachments2(JsonNode attached, List<Thing> things){
    //     for (JsonNode coordinate : attached){
    //         for(Thing thing : things){
    //             if(coordinate.get(0).asInt() == thing.x && coordinate.get(1).asInt() == thing.y){
    //                 attachments.put(thing.x+":"+thing.y,thing);
    //             }
    //         }
    //     }

    //     for (Map.Entry<String, Thing> entry : attachments.entrySet()){
    //         System.out.println("Attachment2: "+entry.getValue());
    //     }
    // }

    public Rotate rotate(Map<String,String> obstacleUpdates){   // obstacleUpdates is temp until bug is found for cell terrain update
       
        System.out.println("Terrain agent: "+map.getTerrain(x, y));
        System.out.println("Terrain west"+map.getTerrain(x-1, y));

        String x_n = String.valueOf(x);
        String y_n = String.valueOf(y-1);
        System.out.println("North: "+obstacleUpdates.get(x_n+":"+y_n));
        String x_w = String.valueOf(x-1);
        String y_w = String.valueOf(y);
        System.out.println("West: "+obstacleUpdates.get(x_w+":"+y_w));
        String x_s = String.valueOf(x);
        String y_s = String.valueOf(y+1);
        System.out.println("South: "+obstacleUpdates.get(x_s+":"+y_s));
        String x_e = String.valueOf(x+1);
        String y_e = String.valueOf(y);
        System.out.println("East: "+obstacleUpdates.get(x_e+":"+y_e));
        System.out.println(x + ":" +y);
        
        Map<String,List<Thing>> quadrants = splitAttachmentsIntoFourQuadrants();             // plane split into 4 quadrants around agent's location (needed to calulate rotate())
        List<Coordinates> setOfCoordinates = new LinkedList<>();
        Boolean [][] canRotate = new Boolean[4][2];
        for(int i=0; i<4; i++){
            canRotate[i][1] = false;
        }
        // if (quadrants.containsKey("quadrant1")){
        //     canRotate[0][0] = true;
        //     List<Thing> quadrant_attachments = quadrants.get("quadrant1");
        //     System.out.println(quadrant_attachments);
        //     setOfCoordinates = calculateIntermediatePointsInQuadrants(quadrant_attachments, 1);
        //     System.out.println("Quadrant 1: ");
        //     for (Coordinates coordinates : setOfCoordinates){
        //         System.out.println(coordinates.toString());
        //         if (!obstacleUpdates.containsKey((coordinates.x+x)+":"+(coordinates.y+y))){
        //              canRotate[0][1] = true;
        //         }   
        //     }

        // }
        // System.out.println();
        if (quadrants.containsKey("quadrant2")){
            canRotate[1][0] = true;
            List<Thing> quadrant_attachments = quadrants.get("quadrant2");
            System.out.println(quadrant_attachments);
            setOfCoordinates = calculateIntermediatePointsInQuadrants(quadrant_attachments, 2);
            System.out.println("Quadrant 2: ");
            for (Coordinates coordinates : setOfCoordinates){
                System.out.println(coordinates.toString());
                // System.out.println("Terrain: "+map.getTerrain(coordinates.x, coordinates.y));
                if (!obstacleUpdates.containsKey((coordinates.x+x)+":"+(coordinates.y+y))){
                    canRotate[1][1] = true;
               }
               
            }
        }
        // System.out.println();
        // if (quadrants.containsKey("quadrant3")){
        //     canRotate[2][0] = true;
        //     List<Thing> quadrant_attachments = quadrants.get("quadrant3");
        //     System.out.println(quadrant_attachments);
        //     setOfCoordinates = calculateIntermediatePointsInQuadrants(quadrant_attachments, 3);
        //     System.out.println("Quadrant 3: ");
        //     for (Coordinates coordinates : setOfCoordinates){
        //         System.out.println(coordinates.toString());
        //         // System.out.println("Terrain: "+map.getTerrain(coordinates.x, coordinates.y));
        //         if (!obstacleUpdates.containsKey((coordinates.x+x)+":"+(coordinates.y+y))){
        //             canRotate[2][1] = true;
        //        }
               
        //     }
        // }
        // System.out.println();
        // if (quadrants.containsKey("quadrant4")){
        //     canRotate[3][0] = true;
        //     List<Thing> quadrant_attachments = quadrants.get("quadrant4");
        //     System.out.println(quadrant_attachments);
        //     setOfCoordinates = calculateIntermediatePointsInQuadrants(quadrant_attachments, 4);
        //     System.out.println("Quadrant 4: ");
        //     for (Coordinates coordinates : setOfCoordinates){
        //         System.out.println(coordinates.toString());
        //         // System.out.println("Terrain: "+map.getTerrain(coordinates.x, coordinates.y));
        //         if (!obstacleUpdates.containsKey((coordinates.x+x)+":"+(coordinates.y+y))){
        //             canRotate[3][1] = true;
        //        }
               
        //     }
        // }

        Rotate rotate = new Rotate();
        // if(canRotate[0][1] == true && canRotate[1][1] == true && canRotate[2][1] == true && canRotate[3][1] == true){
        //     rotate.direction = "ccw";
        // }
        if(canRotate[1][1] == true)
        {
            rotate.direction = "ccw";
        }
        return rotate;
    }

    // public removeLowerYValuesInQuadrant1(){
        
    // }

    public List<Coordinates> combineListsWithoutDuplicates(List<Coordinates> coordinates1, List<Coordinates> coordinates2){

        if (coordinates1.size() >= coordinates2.size()){
            for(Coordinates coordinates : coordinates2){
                if(!coordinates1.contains(coordinates)){
                    coordinates1.add(coordinates);
                }
            }
            return coordinates1;
        }
        else if (coordinates1.size() < coordinates2.size()){
            for(Coordinates coordinates : coordinates1){
                if(!coordinates2.contains(coordinates)){
                    coordinates2.add(coordinates);
                }
            }
            return coordinates2;
        }
        return coordinates1;
    }


    public List<Coordinates> calculateIntermediatePointsInQuadrants(List<Thing> quadrant_attachments, int quadrant){

        List<Coordinates> setOfCoordinates = new LinkedList<>();
        for( Thing attachment : quadrant_attachments){
            if(quadrant == 1 || quadrant == 3){

                 // calculates distance from attachment to agent
                 // For the starting vector, distance to agent will only be the absolute value of the y value
                int distanceToAgent = calculateDistanceToAgent(attachment.x, attachment.y);   
                if(attachment.x == 0){ // starting vector

                    System.out.println("TESTING X2 Y2: "+attachment.x+":"+attachment.y);
                    System.out.println("TESTING X2 Y2: "+attachment.x+":"+(-attachment.y));
                    int [] new_point_after_rotation = calculateNewPointAfter90DegreeRotationCCW(attachment.x, (-attachment.y));
                    System.out.println("TESTING new_point_after_rotation: "+new_point_after_rotation[0]+":"+new_point_after_rotation[1]);

                    setOfCoordinates = calculateIntermediatePointsInQuadrantForStartingVector(distanceToAgent, quadrant);
                    for (Coordinates coordinates : setOfCoordinates){
                        System.out.println("Quadrant "+quadrant+": "+coordinates.toString());
                    }
                }
                else if (attachment.x < 0 || attachment.x > 0){ // Intermediary vector

                    // Calculate equation of a line
                    double m_val_for_start_vector = 0.0;
                    double m_val_for_end_vector = 0.0;
                    m_val_for_start_vector = calculateSlope(0, 0, attachment.x, (-attachment.y));

                    int [] new_point_after_rotation = calculateNewPointAfter90DegreeRotationCCW(attachment.x, (-attachment.y));
                    System.out.println("TESTING new_point_after_rotation: "+new_point_after_rotation[0]+":"+new_point_after_rotation[1]);
                    m_val_for_end_vector = calculateSlope(0, 0, new_point_after_rotation[0], new_point_after_rotation[1]);

                    // Because b will always be zero, equation of line will always be: y = mx 
                    // We can now use this to plug in points when calculating cells that need to be free
                    // For quadrant 1: If y > mx then we know we have gone too far and don't need to include that cell
                    System.out.println("TESTING X2 Y2: "+attachment.x+":"+attachment.y);
                    System.out.println("TESTING X2 Y2: "+attachment.x+":"+(-attachment.y));
                    System.out.println("TESTING Slope start vector: "+m_val_for_start_vector);
                    System.out.println("TESTING Slope end vector: "+m_val_for_end_vector);
                     List<Coordinates> setOfCoordinates2 = calculateIntermediatePointsInQuadrants1And3ForIntermediateVectors(distanceToAgent, m_val_for_start_vector, m_val_for_end_vector,quadrant);
                    for (Coordinates coordinates : setOfCoordinates2){
                        System.out.println(coordinates.toString());
                    }
                }  
            } 
      
            if(quadrant == 2 || quadrant == 4){
                int distanceToAgent = calculateDistanceToAgent(attachment.x, attachment.y);    // calculates distance from attachment to agent
                if(attachment.y == 0){ // Starting vector

                    System.out.println("TESTING X2 Y2: "+attachment.x+":"+attachment.y);
                    System.out.println("TESTING X2 Y2: "+attachment.x+":"+(-attachment.y));
                    int [] new_point_after_rotation = calculateNewPointAfter90DegreeRotationCCW(attachment.x, (-attachment.y));
                    System.out.println("TESTING new_point_after_rotation: "+new_point_after_rotation[0]+":"+new_point_after_rotation[1]);

                    // Distance argument is hardcoded for testing purposes only
                    setOfCoordinates = calculateIntermediatePointsInQuadrantForStartingVector(distanceToAgent, quadrant);
                    for (Coordinates coordinates : setOfCoordinates){
                        System.out.println("Quadrant "+quadrant+": "+coordinates.toString());
                    }
                }

                else if (attachment.y < 0 || attachment.y > 0){  // Intermediary vector

                    // Calculate equation of a line
                    double m_val_for_start_vector = 0.0;
                    double m_val_for_end_vector = 0.0;
                    m_val_for_start_vector = calculateSlope(0, 0, attachment.x, (-attachment.y));

                    int [] new_point_after_rotation = calculateNewPointAfter90DegreeRotationCCW(attachment.x, (-attachment.y));
                    System.out.println("TESTING new_point_after_rotation: "+new_point_after_rotation[0]+":"+new_point_after_rotation[1]);
                    m_val_for_end_vector = calculateSlope(0, 0, new_point_after_rotation[0], new_point_after_rotation[1]);

                    // Because b will always be zero, equation of line will always be: y = mx 
                    // We can now use this to plug in points when calculating cells that need to be free
                    // For quadrant 1: If y > mx then we know we have gone too far and don't need to include that cell
                    System.out.println("TESTING X2 Y2: "+attachment.x+":"+attachment.y);
                    System.out.println("TESTING X2 Y2: "+attachment.x+":"+(-attachment.y));
                    System.out.println("TESTING Slope start vector: "+m_val_for_start_vector);
                    System.out.println("TESTING Slope end vector: "+m_val_for_end_vector);
                     List<Coordinates> setOfCoordinates2 = calculateIntermediatePointsInQuadrants2And4ForIntermediateVectors(distanceToAgent , m_val_for_start_vector, m_val_for_end_vector, quadrant);
                    for (Coordinates coordinates : setOfCoordinates2){
                        System.out.println(coordinates.toString());
                    }
                }  
            }         
        }
        return setOfCoordinates;
    }

    public List<Coordinates> calculateIntermediatePointsInQuadrantForStartingVector(int distance, int quadrant){  
        // Start at (0,0) and work back along the x-axis, point by point
        // At each point on x-axis, work up y-axis (or down if quadrant = 2) at that x coordinate
        // Distance is reduced by one every time we move one point further back along the x-axis
        // Add each point found to array

        // If quadrant = 1: quadrant1, If quadrant = 2: quadrant2, If quadrant = 3: quadrant3, If quadrant = 4: quadrant4
        // Quadrant1: -x, +y
        // Quadrant2: -x, -y
        // Quadrant3: +x, -y
        // Quadrant4: +x, +y
        List<Coordinates> setOfCoordinates = new LinkedList<>();
        int y_distance = distance;
        for(int i = 1; i <= distance; i++){
            Coordinates coordinates = new Coordinates();
            if(quadrant == 1 || quadrant == 2){
                coordinates.x = -i;
                coordinates.y = 0;
            }
            else if(quadrant == 3 || quadrant == 4){
                coordinates.x = i;
                coordinates.y = 0;
            }
            
            setOfCoordinates.add(coordinates);
            y_distance--;
            for(int j = 1; j<= y_distance; j++){
                Coordinates coordinates2 = new Coordinates();  
                if(quadrant == 1){
                    coordinates2.y = j;
                    coordinates2.x = -i;
                }
                else if (quadrant == 2){
                    coordinates2.y = -j;
                    coordinates2.x = -i;
                }
                else if (quadrant == 3){
                    coordinates2.y = -j;
                    coordinates2.x = i;
                }
                else if (quadrant == 4){
                    coordinates2.y = j;
                    coordinates2.x = i;
                }     
                setOfCoordinates.add(coordinates2);
            }
        }
        return setOfCoordinates;
    }

    public List<Coordinates> calculateIntermediatePointsInQuadrants1And3ForIntermediateVectors(int distance, double m_initial_vector, double m_final_vector, int quadrant){
        // Start at (0,0) and work back along the x-axis, point by point
        // At each point on x-axis, work up y-axis at that x coordinate
        // Distance is reduced by one every time we move one point further back along the x-axis
        // Add each point found to array
        List<Coordinates> setOfCoordinates = new LinkedList<>();
        int y_distance = distance;
        for(int i = 1; i <= distance; i++){
            Coordinates coordinates = new Coordinates();
            if(quadrant == 1){
                coordinates.x = -i;
                coordinates.y = 0;
            }
            else if(quadrant == 3){
                coordinates.x = i;
                coordinates.y = 0;
            }
            
            setOfCoordinates.add(coordinates);
            y_distance--;
            for(int j = 1; j <= y_distance; j++){
                // use slope of initial vector & slope of final vector to see if a point is an intermediary point
                if(quadrant == 1){
                    if(j <= (m_initial_vector*-i)){
                        Coordinates coordinates2 = new Coordinates();
                        coordinates2.x = -i;
                        coordinates2.y = j;
                        setOfCoordinates.add(coordinates2);
                    }
    
                    if(-j >= (m_final_vector*-i)){
                        Coordinates coordinates2 = new Coordinates();
                        coordinates2.x = -i;
                        coordinates2.y = -j;
                        setOfCoordinates.add(coordinates2);
                    }
                }
                
                else if(quadrant == 3){
                    if(j <= (m_final_vector*i)){
                        Coordinates coordinates2 = new Coordinates();
                        coordinates2.x = i;
                        coordinates2.y = j;
                        setOfCoordinates.add(coordinates2);
                    }
    
                    if(-j >= (m_initial_vector*i)){
                        Coordinates coordinates2 = new Coordinates();
                        coordinates2.x = i;
                        coordinates2.y = -j;
                        setOfCoordinates.add(coordinates2);
                    }
                }
                
            }
        }

        return setOfCoordinates;
    }

    // Used to Calculate intermediate points in both quadrant 2 & quadrant 4
    public List<Coordinates> calculateIntermediatePointsInQuadrants2And4ForIntermediateVectors(int distance, double m_initial_vector, double m_final_vector, int quadrant){
        // Start at (0,0) and work down (or up if quadrant 4) the y-axis, point by point
        // At each point on y-axis, work along x-axis at that y coordinate
        // Distance is reduced by one every time we move one point further down (or up) the y-axis
        // Add each point found to the list of Coordinates
        List<Coordinates> setOfCoordinates = new LinkedList<>();
        int x_distance = distance;
        for(int i = 1; i <= distance; i++){
            Coordinates coordinates = new Coordinates();
            if(quadrant == 2){
                coordinates.x =  0;
                coordinates.y = -i;    // y always negative with rotation in 2nd quadrant
            }

            else if(quadrant == 4){
                coordinates.x =  0;
                coordinates.y = i;    // y always positive with rotation in 4th quadrant
            }
            
            setOfCoordinates.add(coordinates);
            x_distance--;
            for(int j = 1; j <= x_distance; j++){
                // use slope of initial vector & slope of final vector to see if a point is an intermediary point
                if(quadrant == 2){
                    if(-i <= (m_initial_vector*-j)){
                        Coordinates coordinates2 = new Coordinates();
                        coordinates2.x = -j;
                        coordinates2.y = -i;
                        setOfCoordinates.add(coordinates2);
                    }
    
                    if(-i <= (m_final_vector*j)){
                        Coordinates coordinates2 = new Coordinates();
                        coordinates2.x =  j;
                        coordinates2.y = -i;
                        setOfCoordinates.add(coordinates2);
                    }       
                }  
                
                else if(quadrant == 4){
                    if( i >= (m_initial_vector*j)){
                        Coordinates coordinates2 = new Coordinates();
                        coordinates2.x = j;
                        coordinates2.y = i;
                        setOfCoordinates.add(coordinates2);
                    }
    
                    if( i >= (m_final_vector*-j)){
                        Coordinates coordinates2 = new Coordinates();
                        coordinates2.x =  -j;
                        coordinates2.y =  i;
                        setOfCoordinates.add(coordinates2);
                    }       
                }       
            }
        }

        return setOfCoordinates;
    }

    // This method essentially performs a linear transformation on a given vector
    public int[] calculateNewPointAfter90DegreeRotationCCW(int x_val, int y_val){

        int [][] rotationMatrix = {{0,-1} , 
                                   {1, 0}  };
        int [] newPoint = new int[2];
        newPoint[0] = y_val*-1;
        newPoint[1] = x_val;
        return newPoint;
    }

    public int calculateBValue(int x2, int y2, double slope){    // b should always be zero anyway if line passes through (0,0)
        int b_val = 0;
        return b_val;
    }

    public double calculateSlope(int x1, int y1, int x2, int y2){
        double m = 0.0;
        int top = y2 - y1;
        int bottom = x2 - x1;
        double t = (double) top;
        double b = (double) bottom; 
        m = t/b;
        return m;
    }

    public int calculateDistanceToAgent(int x_coord, int y_coord){
        int distance = 0;
        distance = Math.abs(x_coord) + Math.abs(y_coord);
        return distance;
    }

    public Map<String,List<Thing>> splitAttachmentsIntoFourQuadrants(){
        System.out.println("TESTING findX/YCoords: ");
        Map<String,List<Thing>> quadrants = new TreeMap<>();            // plane split into 4 quadrants around agent's location (needed to calulate rotate())
        /**
         * Quadrants: [1 3]
         *            [2 4]
         */          
        for (Map.Entry<String, Thing> entry : attachments.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
            String key = entry.getKey();
            //Convert string to separate x and y values
            String x_coord = findXCoordinate(key);
            String y_coord = findYCoordinate(key);
            // System.out.println("TESTING findX/YCoords: "+x_coord +", "+y_coord);
            int x_coordinate = Integer.parseInt(x_coord);
            int y_coordinate = Integer.parseInt(y_coord);
            int [] abs_coords = findAbsCoordinates(x_coordinate, y_coordinate);
            int abs_x = abs_coords[0];
            int abs_y = abs_coords[1];
            if(abs_x <= x && abs_y < y){  // If true then we know that this attachment is in the 1st quadrant
                                                                                                                      
                if(quadrants.containsKey("quadrant1")) {
                    List<Thing> things = quadrants.get("quadrant1");
                    things.add(entry.getValue());
                    System.out.println("Quadrant 1: "+abs_x+":"+abs_y);
                }
                else if (!quadrants.containsKey("quadrant1")){
                    List<Thing> things = new LinkedList<>();
                    things.add(entry.getValue());
                    quadrants.put("quadrant1", things);
                    System.out.println("New Quadrant 1: "+abs_x+":"+abs_y);
                }                                                                                           
            }

            else if(abs_x < x && abs_y >= y){  // If true then we know that this attachment is in the 2nd quadrant
                                                                                                                         
                if(quadrants.containsKey("quadrant2")) {
                    List<Thing> things = quadrants.get("quadrant2");
                    things.add(entry.getValue());
                    System.out.println("Quadrant 2: "+abs_x+":"+abs_y);
                }
                else if(!quadrants.containsKey("quadrant2")){
                    List<Thing> things = new LinkedList<>();
                    things.add(entry.getValue());
                    quadrants.put("quadrant2", things);
                    System.out.println("New Quadrant 2: "+abs_x+":"+abs_y);
                }                                                                                           
            }

            else if(abs_x >= x && abs_y > y){  // If true then we know that this attachment is in the 3rd quadrant
                                                                                                                         
                if(quadrants.containsKey("quadrant3")) {
                    List<Thing> things = quadrants.get("quadrant3");
                    things.add(entry.getValue());
                    System.out.println("Quadrant 3: "+abs_x+":"+abs_y);
                }
                else if(!quadrants.containsKey("quadrant3")){
                    List<Thing> things = new LinkedList<>();
                    things.add(entry.getValue());
                    quadrants.put("quadrant3", things);
                    System.out.println("New Quadrant 3: "+abs_x+":"+abs_y);
                }                                                                                           
            }

            else if(abs_x > x && abs_y <= y){  // If true then we know that this attachment is in the 4th quadrant
                                                                                                                         
                if(quadrants.containsKey("quadrant4")) {
                    List<Thing> things = quadrants.get("quadrant4");
                    things.add(entry.getValue());
                    System.out.println("Quadrant 4: "+abs_x+":"+abs_y);
                }
                else if(!quadrants.containsKey("quadrant4")){
                    List<Thing> things = new LinkedList<>();
                    things.add(entry.getValue());
                    quadrants.put("quadrant4", things);
                    System.out.println("New Quadrant 4: "+abs_x+":"+abs_y);
                }                                                                                           
            }

        }    

        return quadrants;
    }

    public String findXCoordinate(String coordinates){
        String s = "";
        for(int i = 0; i < coordinates.length(); i++){
            if (coordinates.charAt(i) == ':'){
                s = coordinates.substring(0, i);
                return s; 
            }
        }
        return s;
    }

    public String findYCoordinate(String coordinates){
        String s = "";
        for(int i = 0; i < coordinates.length(); i++){
            if (coordinates.charAt(i) == ':'){
                s = coordinates.substring(i+1); 
                return s;
            }
        }
        return s;
    }

    public int[] findAbsCoordinates(int x_c, int y_c){
        int [] coord_array = new int[2];
        coord_array [0] = x + x_c;
        coord_array [1] = y + y_c;
        return coord_array;

    }

    public List<Connect> connect(List<Thing> things){
        /**
         * used to connect things attached to agents i.e. cannot connect one agent to another, only the attachments
         * parameters: (otherAgent,x,y)
         * 
         * 1) check to see what are the things attached to the agent
         * 2) For each attachment, check if it there is an 'thing' in an adjacent cell that does not belong to the agent
         *    If this thing is a block then save this info as an affordance (Note: Need to create a Connect class)
         */
        // First implementation: Follow example diagram and assume that each agent must have a block attached to them
        List<Thing> entitiesAndBlocks = new LinkedList<>();
        for(Thing thing : things){
            String x_val = "";
            String y_val = "";
            x_val = String.valueOf(thing.x);
            y_val = String.valueOf(thing.y);
            System.out.println("Entities & blocks");
            if(thing.type.equals("block") || thing.type.equals("entity")){
                if(!attachments.containsKey(x_val+":"+y_val)){
                    if(thing.type.equals("block")){
                        entitiesAndBlocks.add(thing);
                        System.out.println(thing.type);
                    }

                    else if (thing.type.equals("entity")){
                        if(!(thing.x == 0 && thing.y == 0)){
                            entitiesAndBlocks.add(thing);
                            System.out.println(thing.type);
                        }
                    }
                    
                    
                }
            }    
        }

        List<Connect> connects = new LinkedList<>();
        for (Map.Entry<String,Thing> entry : attachments.entrySet()){
            System.out.println("Testing attachments for connect(): "+entry.getKey()+": "+entry.getValue());
            Thing currentAttachment = entry.getValue();
            for(Thing thing : entitiesAndBlocks){
                System.out.println(entry.getValue());
                if(thing.x == currentAttachment.x && (thing.y+1) == currentAttachment.y){   // Located N
                    System.out.println("Location N: "+thing.x+":"+thing.y);
                    Connect connect = new Connect();
                    connect.x = thing.x;
                    connect.y = thing.y;
                    connect.thing = thing;
                    connects.add(connect);
                }
                else if((thing.x+1) == currentAttachment.x && thing.y == currentAttachment.y){  // Located W 
                    System.out.println("Location W: "+thing.x+":"+thing.y);
                    Connect connect = new Connect();
                    connect.x = thing.x;
                    connect.y = thing.y;
                    connect.thing = thing;
                    connects.add(connect);
                }
                else if(thing.x == currentAttachment.x && (thing.y-1) == currentAttachment.y){  // Located S 
                    System.out.println("Location s: "+thing.x+":"+thing.y);
                    Connect connect = new Connect();
                    connect.x = thing.x;
                    connect.y = thing.y;
                    connect.thing = thing;
                    connects.add(connect);
                }
                else if((thing.x-1) == currentAttachment.x && thing.y == currentAttachment.y){  // Located E 
                    System.out.println("Location E: "+thing.x+":"+thing.y);
                    Connect connect = new Connect();
                    connect.x = thing.x;
                    connect.y = thing.y;
                    connect.thing = thing;
                    connects.add(connect);
                }
            }
        }
        return connects;
    }

    public List<Disconnect> disconnect(){
        List<Disconnect> disconnects = new LinkedList<>();
        for (Map.Entry<String,Thing> entry1 : attachments.entrySet()){

            for (Map.Entry<String,Thing> entry2 : attachments.entrySet()){
                Thing thing1 = entry1.getValue();
                Thing thing2 = entry2.getValue();
                if(thing2.x == thing1.x && (thing2.y+1) == thing1.x){   // Located N
                    System.out.println("Location N: "+thing2.x+":"+thing2.y);
                    Disconnect disconnect = new Disconnect();
                    disconnect.attachment1 = thing1;
                    disconnect.attachment2 = thing2;
                    disconnects.add(disconnect);
                }
                else if((thing2.x+1) == thing1.x && thing2.y == thing1.y){  // Located W 
                    System.out.println("Location W: "+thing2.x+":"+thing2.y);
                    Disconnect disconnect = new Disconnect();
                    disconnect.attachment1 = thing1;
                    disconnect.attachment2 = thing2;
                    disconnects.add(disconnect);
                }
                else if(thing2.x == thing1.x && (thing2.y-1) == thing1.y){  // Located S 
                    System.out.println("Location s: "+thing2.x+":"+thing2.y);
                    Disconnect disconnect = new Disconnect();
                    disconnect.attachment1 = thing1;
                    disconnect.attachment2 = thing2;
                    disconnects.add(disconnect);
                }
                else if((thing2.x-1) == thing1.x && thing2.y == thing1.y){  // Located E 
                    System.out.println("Location E: "+thing2.x+":"+thing2.y);
                    Disconnect disconnect = new Disconnect();
                    disconnect.attachment1 = thing1;
                    disconnect.attachment2 = thing2;
                    disconnects.add(disconnect);
                }
            }
        }
        return disconnects;
    }

    public List<Request> request(List<Thing> dispensers, List<Thing> blocks){
        
        List<Request> requests = new LinkedList<>();
        for(Thing dispenser : dispensers){
                boolean blockAndDispenserLocatedOnSameCell = false;
                for(int i = 0; i < blocks.size(); i++){
                    if(dispenser.x == blocks.get(i).x && dispenser.y == blocks.get(i).y){
                        blockAndDispenserLocatedOnSameCell = true;
                    }
                }
                if(!blockAndDispenserLocatedOnSameCell){
                    if(dispenser.x == 0 && dispenser.y == -1){
                        Request request = new Request();
                        request.direction = "n";
                        request.details = dispenser.details;
                        requests.add(request);
                    }
                    if(dispenser.x == 0 && dispenser.y == 1){
                        Request request = new Request();
                        request.direction = "s";
                        request.details = dispenser.details;
                        requests.add(request);
                    }
                    if(dispenser.x == 1 && dispenser.y == 0){
                        Request request = new Request();
                        request.direction = "e";
                        request.details = dispenser.details;
                        requests.add(request);
                    }
                    if(dispenser.x == -1 && dispenser.y == 0){
                        Request request = new Request();
                        request.direction = "w";
                        request.details = dispenser.details;
                        requests.add(request);
                    }
                }
        }
        return requests;
    }

    public Submit submit(){
       
         Submit submit = new Submit();
         if (map.getTerrain(x,y).equals("goal")){
             submit.taskName = "Enter taskname";     // Submit is possible
         } 
         return submit;        // If submit.taskName = null then submit is not possible
    }

    public Clear clear(List<Thing> things, List<String> obstacles){
       
        Clear clear = new Clear();
        clear.obstacles = obstacles;
        for(Thing thing : things){
            if(thing.type.equals("entity") && thing.details.equals("B")){
                clear.entities.add(thing);
            }

            /**
             * 1) block is attached to another block
             * 2) block is attached to an entity
             * 3) block is attached to nothing
             */
            // else if(thing.type.equals("block")){
            //     for(thing.)
            // }

            // else if(thing.type.equals("block")){
            //     for(Thing thing2 : things){
            //         if(thing2.x == thing.x && thing2.y == (thing.y+1)){  // north
            //             List<Thing> north = map.getThings(x, y+thing2.y);
            //             if(north != null){

            //             }
            //         }    
            //     }
            // }
        }
        return clear;
    }

    public Accept accept(){

        Accept accept = new Accept();
        if (computeManhattanDistance(2)){
            accept.taskName = "Enter taskname";   // A taskboard exists with distance <= 2
        }
        return accept;     // accept.taskName = null if no taskboard exists with distance <= 2
    }

    public boolean computeManhattanDistance(int distance){

        return computeLeftTriangle(distance) || computeRightTriangle(distance);
    }

    public boolean computeLeftTriangle(int distance){
       
        int abs_x = x-distance;      // starting point of algorithm
        int abs_y = y;
        int y_distance = -1;
        for(int x_axis = 0; x_axis <= distance; x_axis++){
            String x_val = String.valueOf(abs_x + x_axis);
            String y_val = String.valueOf(abs_y);
            if (taskboards.containsKey(x_val+":"+y_val)){
                return true;
            }
            y_distance += 1;
            for(int y_axis = 1; y_axis <= y_distance; y_axis++){ 
                String y_val_north = String.valueOf(abs_y + y_axis); 
                String y_val_south = String.valueOf(abs_y - y_axis); 
                if (taskboards.containsKey(x_val+":"+y_val_north) || taskboards.containsKey(x_val+":"+y_val_south)){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean computeRightTriangle(int distance){
        int abs_x = x+distance;      // starting point of algorithm
        int abs_y = y;
        int y_distance = -1;
        for(int x_axis = 0; x_axis < distance;  x_axis++){
            String x_val = String.valueOf(abs_x - x_axis);
            String y_val = String.valueOf(abs_y);
            if (taskboards.containsKey(x_val+":"+y_val)){
                return true;
            }
            y_distance += 1;
            for(int y_axis = 1; y_axis <= y_distance; y_axis++){
                String y_val_north = String.valueOf(abs_y + y_axis); 
                String y_val_south = String.valueOf(abs_y - y_axis); 
                if (taskboards.containsKey(x_val+":"+y_val_north) || taskboards.containsKey(x_val+":"+y_val_south)){
                    return true;
                }
            }
        }
        return false;
    }

     /**
     * Barry's code
     * END
     */

    //****************************************************************************
    // ModelListener linked methods
    //****************************************************************************
    public void register(ModelListener listener) {
        listeners.add(listener);
    }

    public void notify(String type, Object[] params) {
        for (ModelListener listener : listeners) {
            listener.receive(type, params);
        }
    }

    //****************************************************************************
    // Methods below used understand the agents coordinate system
    //****************************************************************************
    public boolean isMe(Thing thing) {
        return thing.type.equals(ENTITY) && thing.details.equals(team) && thing.x == x && thing.y == y;
    }
    
    public boolean inBounds(int x, int y) {
        return min_x <= x && min_y <= y && max_x >= x && max_y >= y;
    }

    public GridMap getMap() {
        return map;
    }

    //****************************************************************************
    // Convert relative agent coords to absolute agent coords
    //****************************************************************************
    public int abs_x(int x) {
        return this.x+x;
    }

    public int abs_y(int y) {
        return this.y+y;
    }

    //****************************************************************************
    // Methods to update the map
    //****************************************************************************
    public void updateVisibleArea(Map<String, String> terrainUpdates, Map<String, List<Thing>> thingUpdates) {
        for(int layer=0; layer<vision; layer++) {
            for (int i=-layer;i<=layer; i++) {
                updateCell(x+i, y+layer-vision, terrainUpdates, thingUpdates);
                updateCell(x+i, y+vision-layer, terrainUpdates, thingUpdates);
            }
        }
        for (int i=-vision;i<=vision; i++) {
            updateCell(x+i, y, terrainUpdates, thingUpdates);
        }
    }

    private void updateCell(int x, int y, Map<String,String> terrainUpdates, Map<String, List<Thing>> thingUpdates) {
        String key = x+":"+y;
        map.setTerrain(terrainUpdates.computeIfAbsent(key, s -> CLEAR), x, y);
        map.setThings(thingUpdates.computeIfAbsent(key, s -> new LinkedList<>()), x, y);
        if (x < min_x) min_x = x;
        if (x > max_x) max_x = x;
        if (y < min_y) min_y = y;
        if (y > max_y) max_y = y;
    }

    public List<Thing> getThings(String type, String details) {  // returns all the things of that type e.g. (dispenser, b1)
                                                                // will return all dispensers that give b1 blocks
        List<Thing> list = new LinkedList<>();
        List<Thing> thingList = things.get(type);
        if (thingList != null) {
            for (Thing thing : thingList) {
                if (thing.details.equals(details)) {
                    list.add(thing);
                }
            }
        }
        return list;
    }
    //****************************************************************************
    // Methods to record the last action
    //****************************************************************************
    public boolean hasLastAction() {
        return lastAction != null;
    }

    public void applyLastAction() {
        actionLog.add(new ActionLogEntry(step-1, lastAction));
        
        lastAction = null;
    }

    public void recordMove(String dir) {
        if (dir.equals("n")) {
            y--;
        } else if (dir.equals("s")) {
            y++;
        } else if (dir.equals("e")) {
            x++;
        } else if (dir.equals("w")) {
            x--;
        }                    
    }


    //****************************************************************************
    // Graph Visualisation
    //****************************************************************************
    public String toString() {
        StringBuilder buf = new StringBuilder();
        toString(buf);
        return buf.toString();
    }

    public void toString(StringBuilder buf) {
        buf.append("Location: ("+x+","+y+")\n");
        buf.append("Min: ("+min_x+","+min_y+") / Max: (" + max_x + "," + max_y + "):\n");
        for (int j=min_y; j<=max_y;j++) {
            if (j==min_y) {
                buf.append("    ");
                for (int i=min_x; i<=max_x;i++) {
                    buf.append(" | ");
                    padLabel(buf, i);
                }
                buf.append(" |\n");
            }

            padLabel(buf, j);
            buf.append(" ");
            for (int i=min_x; i<=max_x;i++) {
                displayCell(buf, i, j);
            }
            buf.append("|\n");
        }

        buf.append("\n\nPlayers:");
        for (Player player : players.values()) {
            buf.append(" " + player.name + "(" + player.x + "," + player.y + ")");
        }
    }

    private void padLabel(StringBuilder buf, int label) {
        if (label < 0) {
            if (label > -100) buf.append(" ");
            if (label > -10) buf.append(" ");
            
        } else {
            buf.append(" ");
            if (label < 100) buf.append(" ");
            if (label < 10) buf.append(" ");
        }
        buf.append(label);
    }

    private void displayCell(StringBuilder buf, int i, int j) {
        buf.append("| ");
        String terrain = map.getTerrain(i, j);
        if (terrain == null) {
            buf.append("U    ");
            return;
        }

        int padding = 4;

        // Display "Terrain"
        String icon = terrainViewMap.get(terrain);
        buf.append(icon == null ? terrain:icon);

        // Display "Things"
        for (Thing thing : map.getThings(i, j)) {
            if (thing.type.equals(ENTITY) || thing.type.equals(BLOCK)) {
                buf.append(thing.details);
                padding -= thing.details.length();
            } else {
                buf.append(thingsViewMap.get(thing.type));
                padding--;            }

        }

        // Display "my location"
        if (i == x && j == y) {
            buf.append("X");
            padding--;
        }

        // Pad...
        while (padding > 0) {
            buf.append(" ");
            padding--;
        }
    }
}