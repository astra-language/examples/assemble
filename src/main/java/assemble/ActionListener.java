package assemble;

import assemble.ModelListener;
import assemble.ActionModel;

public class ActionListener implements ModelListener {

private Model model;

public ActionListener(Model model){
      this.model=model;
}

      public void receive(String type, Object[] params){

            System.out.println("TESTING");
            if (type.equals("request-action")){

                  System.out.println("Action Listener has Received Action Event");
                  createActionModel();
                  model.notify("action-modelled", new Object[] {});
            }      
      }

      public void createActionModel(){

            
      }
      
}
