package assemble;

import java.util.Arrays;

import core.Action;
import core.MassimClient;

public class Test {
    Model model = new Model();
    MassimClient client = new MassimClient(model);

    public Test() {
        model.register(new ModelListener(){
            @Override
            public void receive(String type, Object[] params) {
                System.out.println("event: " + type + "(" + Arrays.toString(params) + ")");

                if (type.equals("move")) {
                    move(params);
                }
            }
        });
        client.registerHandler("sim-start", new SimStartHandler(model));
        client.registerHandler("request-action", new RequestActionHandler(model));
    }

    public void move(Object[] params) {
        model.lastAction = new Action(model.actionId, "move", params);
        client.act(model.lastAction);
    }

    public void run() {
        client.connect("localhost", 12300);
        client.authenticate("agentA12", "1");
        client.start();
    }
    
    public static void main(String[] args) {
        Test test = new Test();
        test.run();
    }
}