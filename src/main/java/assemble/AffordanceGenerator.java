package assemble;

import java.util.List;

import affordances.Affordance;

public interface AffordanceGenerator {
    void generate(Model model, List<Affordance> affordances);
}
