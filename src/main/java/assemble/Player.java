package assemble;

public class Player {
    public String name;
    public int x;
    public int tx;
    public int y;
    public int ty;
    public int step;

    public Player(String name, int x, int y, int tx, int ty) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.tx = tx;
        this.ty = ty;
    }

    public void move(String dir) {
        switch (dir) {
            case "n":
            y--;
            break;
            case "s":
            y++;
            break;
            case "e":
            x++;
            break;
            case "w":
            x--;
            default:
            // never called
        }                    
    }    

    public String toString() {
        return name + "(" + x + "," + y + ")";
    }
}