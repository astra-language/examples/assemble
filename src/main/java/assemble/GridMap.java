package assemble;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import assemble.Cell;


/**
 * The core model of the map (centered in the 2D array)
 */
public class GridMap {
    public static final int SIZE = 2001;
    private Cell[][] grid;
    private Map<String, List<Thing>> thingMap = new TreeMap<>();
    
    public GridMap() {
        grid = new Cell[SIZE][SIZE];
    }

    /**
     * START 
     * Barry's code
     */

     public void setCell(Thing thing){

        Cell cell = getCell(thing.x, thing.y);
        cell.thing = thing;
     }


     /**
     * END 
     * Barry's code
     */

    public void setTerrain(String type, int x, int y) {
        // System.out.println("("+x+","+y+")="+type);
        getCell(x, y).type = type;
    }

    public Cell getCell(int x, int y) {
        Cell cell = grid[x+SIZE/2][y+SIZE/2];
        if (cell == null) {
            grid[x+SIZE/2][y+SIZE/2] = cell = new Cell(null, -1, x, y);
        }
        return cell;

    }
    public String getTerrain(int x, int y) {
        return getCell(x, y).type;
    }
    

    private String key(int x, int y) {
        return (x+SIZE/2)+":"+(y+SIZE/2);
    }

    public void setThings(List<Thing> things, int x, int y) {
        thingMap.put(key(x,y), things);
    }

    public List<Thing> getThings(int x, int y) {
        return thingMap.get(key(x,y));
    }

    public boolean isFree(int x, int y) {
        String terrain = getTerrain(x, y);
        if (terrain == null) return false;
        if (terrain.equals("clear") || terrain.equals("goal")) {
            return getThings(x, y).isEmpty();
        }
        return false;
    }

    // TODO: Type of Dispenser
    public boolean isDispenser(int x, int y){
        Cell cell = getCell(x,y);
        return cell.type.equals("dispenser");
    }
    
}