package assemble;

import java.util.Set;
import java.util.TreeSet;

public class ExploreGrid {
    private static final int SIZE = 3;
    private static final int[][] OPTIONS = {new int[] {0, -SIZE}, new int[] {SIZE, 0}, new int[] {0, SIZE}, new int[] {-SIZE, 0}};
    private int[] current;
    private int[] home;
    private Set<String> explored = new TreeSet<>();
    private Set<String> unexplored = new TreeSet<>();
    
    public ExploreGrid(int x, int y) {
        home = new int[] {x, y};
        current = new int[]{x,y};
        explored.add(key(current));
    }

    public int addOptions(int x, int y) {
        int count = 0;
        for (int[] option : OPTIONS) {
            int[] location = new int[] {x+option[0],y+option[1]};
            String key = key(location);
            if (!explored.contains(key) && !unexplored.contains(key)) {
                unexplored.add(key);
                count++;
            }
        }
        return count;
    }

    public int[] selectTarget() {
        int distanceToHome = GridMap.SIZE;
        int distanceToCurrent = GridMap.SIZE;

        int[] selected = null;
        for (String option : unexplored) {
            int[] target = coords(option);
            int distance = distance(target, home);
            if (distance < distanceToHome) {
                distanceToHome = distance;
                selected = target;
                distanceToCurrent = distance(target, current);
            }
            if (distance == distanceToHome) {
                distance = distance(target, current);
                if (distance < distanceToCurrent) {
                    distanceToCurrent = distance;
                    selected = target;
                }
            }
        }
        return selected;
    }

    public boolean markVisited(int x, int y) {
        String key = key(x, y);
        if (unexplored.remove(key)) {
            explored.add(key);
            return true;
        }
        return false;
    }

    private int[] createOption(int[] option) {
        int[] location = new int[] {current[0],current[1]};
        location[0]+=option[0];
        location[1]+=option[1];
        return location;
    }

    private int[] coords(String key) {
        int[] coords = new int[2];
        int i = 0;
        for (String coord : key.split(":")) {
            coords[i++] = Integer.parseInt(coord);
        }
        return coords;
    }

    private int distance(int[] source, int[] target) {
        return (int) Math.sqrt((double) (source[0]-target[0])*(source[0]-target[0]) + (source[1]-target[1])*(source[1]-target[1]));
    }

    private String key(int[] coords) {
        return coords[0]+":"+coords[1];
    }

    private String key(int x, int y) {
        return x+":"+y;
    }

    public boolean hasOptions() {
        return !unexplored.isEmpty();
    }
}