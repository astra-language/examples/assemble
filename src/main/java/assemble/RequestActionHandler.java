package assemble;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.JsonNode;

import affordances.Affordance;
import core.MessageHandler;

public class RequestActionHandler implements MessageHandler {
    public static final String NEW_THING = "new-thing";
    public static final String MISSING_THING = "missing-thing";
    public static final String REQUEST_ACTION = "request-action";
    public static final String LAST_ACTION = "last-action";

    private static String[] TERRAIN_TYPES = new String[] {"obstacle", "goal"};

    private Model model;

    private Map<String, Task> taskMap = new TreeMap<>();

    public RequestActionHandler(Model model) {
        this.model = model;
    }

    @Override
    public void handle(JsonNode message) {
        model.message = message;

        JsonNode content = message.get("content");
        JsonNode percept = content.get("percept");

        // ------------------------------------------------------------------------------------
        // Update the model based on the last action...
        // ------------------------------------------------------------------------------------
        String lastActionResult = percept.get("lastActionResult").asText();

        // Notify the listeners 
        model.notify(LAST_ACTION, new Object[] {percept.get("lastAction").asText(), percept.get("lastActionParams"), lastActionResult});
        
         // ------------------------------------------------------------------------------------
        // UPDATE Model
        // ------------------------------------------------------------------------------------
        model.step = content.get("step").asInt();
        model.actionId = content.get("id").asInt();
        

        // Update model with last action
        if (lastActionResult.equals("success") && model.hasLastAction()) {
            System.out.println("Applying last action...");

            if (model.lastAction.type.equals("move")) {
                model.recordMove(model.lastAction.p[0].toString());
            }

            //If lastAction is attach or detach then update attachments list
            else if(model.lastAction.type.equals("attach")){ 
                List<Thing> things = getThingDataFromAdjacentCells(percept.get("things"));
                model.updateAttachments(things, model.lastAction.p[0].toString());
            }

            else if(percept.get("lastAction").asText().equals("detach")){
                model.deleteAttachment(model.lastAction.p[0].toString());
            }
            model.applyLastAction();
            
        }
        System.out.println("Attached: "+percept.get("attached"));
        System.out.println("Agent location: "+model.x+":"+model.y);
        System.out.println("Tasks: "+percept.get("tasks"));
        System.out.println("TESTING RAH Terrain data: "+percept.get("terrain"));
        Map<String, String> terrainUpdates = getTerrainData(percept.get("terrain"));
        Map<String, List<Thing>> thingUpdates = getThingData(percept.get("things")); 
        model.updateVisibleArea(terrainUpdates, thingUpdates);
        updateTasks(percept);
        
        // model.findActions();

        // Update taskboard list
        List<Thing> taskboardUpdates = getThingDataFromAllCells(percept.get("things"));
        model.updateTaskboards(taskboardUpdates);

        /**
         * Send model attachment info
         * 
         * model.updateAttachments(percept.("attached")); 
         */

        List<Affordance> affordances = new LinkedList<>();
        for (AffordanceGenerator generator: Model.generators) {
            generator.generate(model, affordances);
        }

      //    Move
      //    List<Move> moves = new LinkedList<>();
      //    moves = model.move();
      //    System.out.println("-----------AFFORDANCES-----------");
      //    for(Move move : moves){
      //        System.out.println("Move: "+move.direction);
      //    }

      //   Attach
      //   List<Thing> things = new LinkedList<>();
      //   things = getThingDataFromAdjacentCells(percept.get("things"));
      //   List<Attach> attaches = new LinkedList<>();
      //   attaches = model.attach(things,percept.get("attached"));
      //   for(Attach attach : attaches){
      //       System.out.println("Attach: "+attach.direction);
      //   }
      
      //   Detach
      //   List<Detach> detaches = model.detach();
      //   for(Detach detach : detaches){
      //       System.out.println("Detach: "+detach.direction);
      //   }

      //   Request
      //   List<Thing> thingData = getThingDataFromAdjacentCells(percept.get("things"));
      //   List<Thing> dispensers = getDispensers(thingData);
      //   List<Thing> blocks = getBlocks(thingData);
      //   List<Request> requests = new LinkedList<>();
      //   requests = model.request(dispensers, blocks);
      //   for(Request request : requests){
      //       System.out.println("Request: "+request.direction);
      //       System.out.println("Type: "+request.details);
      //   }

        // Accept
      //   List<Thing> t = model.getThings("taskboard", "");
      //   System.out.println("\nTaskboards: "+t);
      //   Accept accept = model.accept();
      //   System.out.println("Accept: "+accept.taskName);

        // Submit
      //   Submit submit = model.submit();
      //   System.out.println("Submit: "+submit.taskName);
       
        // Rotate
        // System.out.println("Attached items: "+percept.get("attached"));
        // Map<String, String> terrainUpdates2 = getTerrainData2(percept.get("terrain"));
        // model.updateAttachments2(percept.get("attached"), getThingDataFromAllCells(percept.get("things")));
        // Rotate rotate = model.rotate(terrainUpdates2);
        //     System.out.println("Rotate: "+rotate.direction);

        //       /**
        //  * testing code for affordances: connect
        //  */
        // List<Thing> thingsForConnect = getThingDataForConnect(percept.get("things"));
        // List<Connect> connects = model.connect(thingsForConnect);
        // System.out.println("-----------AFFORDANCES-----------");
        // for(Connect connect : connects){
        //     System.out.println("Connect: "+connect.x+":"+connect.y);
        // }
        // System.out.println("-----------AFFORDANCES-----------");
        // System.out.println("\n"); 

        //       /**
        //  * testing code for affordances: disconnect
        //  */
        // List<Disconnect> disconnects = model.disconnect();
        // System.out.println("-----------AFFORDANCES-----------");
        // for(Disconnect disconnect : disconnects){
        //     System.out.println("Disconnect: "+disconnect.attachment2.x+":"+disconnect.attachment2.y);
        // }
        // System.out.println("-----------AFFORDANCES-----------");
        // System.out.println("\n"); 

              /**
         * testing code for affordances: disconnect
         */
        // List<Thing> clearThings = getThingDataForConnect(percept.get("things"));
        // Clear clear = model.clear(clearThings);
        // System.out.println("-----------AFFORDANCES-----------");
        // for(Thing obstacle : clear.obstacles){
        //     System.out.println("Obstacle: "+obstacle.x+":"+obstacle.y);
        // }
        // for(Thing entity : clear.entities){
        //     System.out.println("Obstacle: "+entity.x+":"+entity.y);
        // }
        // System.out.println("-----------AFFORDANCES-----------");
        // System.out.println("\n"); 

        // Notify any listeners of the next move...
        model.notify(REQUEST_ACTION, new Object[] {model.actionId, model.step});

    }

    /**
     * START
     * Barry's code
     *
     */

    public Coordinates convertDirectionToCoordinates(String direction){
        Coordinates coordinates = new Coordinates();
        if(direction.equals("n")){
            coordinates.x = 0;
            coordinates.y = -1;
        }

        else if(direction.equals("s")){
            coordinates.x = 0;
            coordinates.y = 1;
        }

        else if(direction.equals("e")){
            coordinates.x = 1;
            coordinates.y = 0;
        }

        else if(direction.equals("w")){
            coordinates.x = -1;
            coordinates.y = 0;
        }

        return coordinates; 
    }

    public List<Thing> getDispensers(List<Thing> things){
        List<Thing> dispensers = new LinkedList<>();
        for(Thing thing : things){
            if(thing.type.equals("dispenser")){
                dispensers.add(thing);
            }
        }
        return dispensers;
    }

    public List<Thing> getBlocks(List<Thing> things){
        List<Thing> blocks = new LinkedList<>();
        for(Thing thing : things){
            if(thing.type.equals("block")){
                blocks.add(thing);
            }     
        }
        return blocks;
    }

    public List<Thing> getThingDataFromAdjacentCells(JsonNode thingNode){
        List<Thing> things = new LinkedList<>();
        for (JsonNode node : thingNode) {
            Thing thing = new Thing(node);
            if( (thing.x == 0 && thing.y == -1) || (thing.x == 0 && thing.y == 1) || (thing.x == 1 && thing.y == 0) ||
                    (thing.x == -1 && thing.y == 0) ){
                        things.add(thing);
                    }
        }
        return things;
    }

    public List<Thing> getThingDataFromAllCells(JsonNode thingNode){
        List<Thing> things = new LinkedList<>();
        for (JsonNode node : thingNode) {
            Thing thing = new Thing(node);
            things.add(thing);
        }
        return things;
    }

     /**
      *  Barry's code
      *  END
      */

    private void updateTasks(JsonNode percept) {
        Map<String, Task> newTaskMap = new TreeMap<>();
        for (JsonNode taskNode : percept.get("tasks")) {
            Task task = new Task(taskNode);
            newTaskMap.put(task.name, task);
            if (taskMap.remove(task.name) == null) {
                model.notify("new-task", new Object[] {task.name});
            }
        }
        for (Entry<String, Task> entry : taskMap.entrySet()) {
            model.notify("lost-task", new Object[] {entry.getKey()});
        }
        taskMap = newTaskMap;
        model.taskMap = newTaskMap;
    }

    private Map<String, String> getTerrainData(JsonNode terrainNode) {
        Map<String, String> terrainUpdates = new TreeMap<>();
        for (String terrain : TERRAIN_TYPES) {
            if (terrainNode.has(terrain)) {
                for (JsonNode coordinate : terrainNode.get(terrain)) {
                    terrainUpdates.put(model.abs_x(coordinate.get(0).asInt())+":"+model.abs_y(coordinate.get(1).asInt()), terrain);
                }
            }
        }
        return terrainUpdates;
    }

    /**START
     * Barry's code
     */
    private Map<String, String> getTerrainData2(JsonNode terrainNode) { // Needed due to bug 
        Map<String, String> terrainUpdates = new TreeMap<>();
       
            if (terrainNode.has("obstacle")) {
                for (JsonNode coordinate : terrainNode.get("obstacle")) {
                    terrainUpdates.put(model.abs_x(coordinate.get(0).asInt())+":"+model.abs_y(coordinate.get(1).asInt()), "obstacle");
                    System.out.println("Terrain coord: "+coordinate);
                    System.out.println(model.abs_x(coordinate.get(0).asInt())+":"+model.abs_y(coordinate.get(1).asInt()));
                }
            }
        return terrainUpdates;
    }
    private List<String> getObstacleData(JsonNode terrainNode) { // relative coordinates
        List<String> obstacles = new LinkedList<>();
       
            if (terrainNode.has("obstacle")) {
                for (JsonNode coordinate : terrainNode.get("obstacle")) {
                    String x_val = String.valueOf(coordinate.get(0).asInt());
                    String y_val = String.valueOf(coordinate.get(1).asInt());
                    obstacles.add(x_val+":"+y_val);
                }
            }
        return obstacles;
    }

    /**Barry's code
     * END
     */
 
    private Map<String, List<Thing>> getThingData(JsonNode thingNode) {
        List<Thing> newMovedTeammates = new LinkedList<>();
        List<Thing> newTeammates = new LinkedList<>();
        model.things.clear();                // Why do all the things get cleared?
        Map<String, List<Thing>> thingUpdates = new TreeMap<>();
        for (JsonNode node : thingNode) {
            Thing thing = new Thing(model, node);
            if (!model.isMe(thing)) {
                thingUpdates.computeIfAbsent(thing.x+":"+thing.y, s-> new LinkedList<>()).add(thing);   //for multiple things on one location?
                // Special - keep a log of entities from my team
                if (thing.type.equals(Model.ENTITY) && thing.details.equals(model.team)) {
                    newTeammates.add(thing);
                    if (!model.teammates.contains(thing)) {
                        newMovedTeammates.add(thing);
                    }
                }
                model.things.computeIfAbsent(thing.type, s->new LinkedList<>()).add(thing);
            }
        }
        model.teammates = newTeammates;
        model.movedTeammates = newMovedTeammates;
        return thingUpdates;
    }
}
