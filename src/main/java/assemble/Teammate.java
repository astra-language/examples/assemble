package assemble;

public class Teammate {
    public String name;
    public int x;
    public int y;

    public Teammate(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }
    
}