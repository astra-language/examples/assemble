package assemble;

import com.fasterxml.jackson.databind.JsonNode;

import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;

public class Utils {
    public static Term asString(JsonNode node) {
        return Primitive.newPrimitive(node.asText());
    }

    public static Term asInt(JsonNode node) {
        return Primitive.newPrimitive(node.asInt());
    }

    public static Term asLong(JsonNode node) {
        return Primitive.newPrimitive(node.asLong());
    }

    public static ListTerm asList(JsonNode node) {
        ListTerm params = new ListTerm();
        for (int i=0; i < node.size(); i++) {
            JsonNode param = node.get(i);
            if (param.isTextual()) {
                params.add(asString(param));
            } else if (param.isInt()) {
                params.add(asInt(param));
            } else {
                System.out.println("Param not handled: " + param);
                System.exit(1);
            }
        }
        return params;
    }    
}