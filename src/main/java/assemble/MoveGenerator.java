package assemble;

import java.util.List;
import java.util.LinkedList;
import affordances.Move;
import affordances.Affordance;

public class MoveGenerator implements AffordanceGenerator {
    @Override
    public void generate(Model model, List<Affordance> affordances) {
            
            if(model.map.isFree(model.x,model.y-1)){
                Move move = new Move();
                move.direction = "n";
                affordances.add(move);
            }
            if(model.map.isFree(model.x,model.y+1)){
                Move move = new Move();
                move.direction = "s";
                affordances.add(move);
            }
            if(model.map.isFree(model.x+1,model.y)){
                Move move = new Move();
                move.direction = "e";
                affordances.add(move);
            }
            if(model.map.isFree(model.x-1,model.y)){
                Move move = new Move();
                move.direction = "w";
                affordances.add(move);
            }
        
    }
    
}
