package assemble;

public interface ModelListener {
    void receive(String type, Object[] params);
}