package assemble;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

public class Task {
    public String name;
    public int deadline;
    public int reward;
    public List<Thing> requirements = new LinkedList<>();

    public Task(){}

    public Task(JsonNode node) {
        this.name = node.get("name").asText();
        this.deadline = node.get("deadline").asInt();
        this.reward = node.get("reward").asInt();
        for (JsonNode reqNode : node.get("requirements")) {
            requirements.add(new Thing(reqNode));
        }
    }
}