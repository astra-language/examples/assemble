package assemble;

import java.util.ArrayList;

public class Cell {
    public static final String CLEAR = "clear";

    public int step;         // step that cell was discovered
    public String type;      // terrain type i.e. empty, goal or obstacle
    public int x;
    public int y;
    public java.util.List<Thing> things = new ArrayList<>();  // Entity, Block, Dispenser or Marker ??
    public Thing thing;         // Entity, Block, Dispenser or Marker

    public Cell(String type, int step, int x, int y) {
        this.type = type;
        this.step = step;
        this.x = x;
        this.y = y;
    }

    public boolean is(String type) {
        return this.type.equals(type);
    }
    public boolean isFree() {
        return this.type.equals(CLEAR) && things.isEmpty();
    }
}