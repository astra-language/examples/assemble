package affordances;

public class Detach extends Affordance {
      
      public Detach() {
            super.type = "detach";
      }

      public String direction;    // n,s,e,w 
      public String detachType;   // Entity, Block ... ?
      
}
