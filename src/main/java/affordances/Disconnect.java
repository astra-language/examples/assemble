package affordances;

import assemble.Thing;

public class Disconnect extends Affordance {
      
      public Disconnect() {
            super.type = "disconnect";
      }

      public Thing attachment1;
      public Thing attachment2;
}
