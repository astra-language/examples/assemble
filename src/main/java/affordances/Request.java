package affordances;

public class Request extends Affordance {
      
      public Request() {
            super.type = "request";
      }

      public String direction;    // n,s,e or w
      public String details;      // type of block
}
