package affordances;

public class Submit extends Affordance {
      
      public Submit() {
            super.type = "submit";
      }

      public String taskName;
}
