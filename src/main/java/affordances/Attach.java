package affordances;

public class Attach extends Affordance {
      
      public Attach() {
            super.type = "attach";
      }

      public String direction;    // n,s,e,w
      public String attachType;    // Block or Entity 
      public String details; 
}
