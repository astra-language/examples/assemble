package affordances;

public class Move extends Affordance {
      
      public Move() {
            super.type = "move";
      }

      public String direction;    // n,s,e, or w
}
