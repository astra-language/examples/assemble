package affordances;

import java.util.LinkedList;
import java.util.List;
import assemble.Thing;

public class Clear extends Affordance {
      
      public Clear () {
            super.type = "clear";
            obstacles = new LinkedList<>();
            entities = new LinkedList<>();
            blocks = new LinkedList<>();
      }

      public List<String> obstacles;
      public List<Thing> entities;
      public List<Thing> blocks;
}
