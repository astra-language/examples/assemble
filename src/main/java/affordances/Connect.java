package affordances;

import assemble.Thing;

public class Connect extends Affordance {

      public Connect () {
            super.type = "connect";
      }

      public String agent;
      public int x;
      public int y;
      public Thing thing;
      
}
