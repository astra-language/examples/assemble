import java.util.Map;
import java.util.TreeMap;

import assemble.ExploreGrid;
import astra.core.ActionParam;
import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;

public class Explore extends Module {
    private Map<String, ExploreGrid> grids = new TreeMap<>();

    @ACTION
    public boolean create(String home, int x, int y) {
        ExploreGrid exploreGrid = new ExploreGrid(x,y);
        exploreGrid.addOptions(x, y);
        grids.put(home, exploreGrid);
        return true;
    }

    @ACTION
    public boolean selectTarget(String home, ActionParam<Integer> x, ActionParam<Integer> y) {
        int[] coords = grids.get(home).selectTarget();
        x.set(coords[0]);
        y.set(coords[1]);
        return true;
    }

    @ACTION
    public boolean markVisited(String home, int x, int y) {
        if (grids.get(home).markVisited(x,y)) {
            grids.get(home).addOptions(x, y);
            return true;
        }
        return false;
    }

    @FORMULA
    public Formula hasTargets(String home) {
        return grids.get(home).hasOptions() ? Predicate.TRUE:Predicate.FALSE;
    }
}