import static assemble.Utils.asList;

import java.awt.EventQueue;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.databind.JsonNode;

import assemble.GradientMap;
import assemble.Model;
import assemble.Player;
import assemble.RequestActionHandler;
import assemble.Thing;
import astra.core.ActionParam;
import astra.core.Agent;
import astra.core.Module;
import astra.event.Event;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.reasoner.Unifier;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;
import core.Action;
import core.MassimClient;
import event.SimEvent;
import event.SimEventUnifier;
import event.ThingEvent;
import event.ThingEventUnifier;
import gui.JPlayerGui;

public class Massim extends Module {
    public static final Primitive<String> NORTH = Primitive.newPrimitive("n");
    public static final Primitive<String> SOUTH = Primitive.newPrimitive("s");
    public static final Primitive<String> EAST = Primitive.newPrimitive("e");
    public static final Primitive<String> WEST = Primitive.newPrimitive("w");

    static {
        Unifier.eventFactory.put(SimEvent.class, new SimEventUnifier());
        Unifier.eventFactory.put(ThingEvent.class, new ThingEventUnifier());
    }

    // private MassimClient client = new MassimClient();
    private MassimClient client;
    private Random random = new Random();
    private JPlayerGui gui;
    private Model model;

    public Massim() {
        client.registerHandler(MassimClient.SIM_START, message -> {
                model = new Model();
                client = new MassimClient(model);      // testing purposes

                // Create a new model (we need to loose everything)
                model.message = message;

                model.register((type, params) -> {
                    if (type.equals(RequestActionHandler.MISSING_THING)) {
                        Thing thing = (Thing) params[0];
                        int x = (int) params[1];
                        int y = (int) params[2];
                        agent.addEvent(new ThingEvent(
                            ThingEvent.REMOVED,
                            Primitive.newPrimitive(thing.type),
                            Primitive.newPrimitive(thing.details),
                            Primitive.newPrimitive(x),
                            Primitive.newPrimitive(y)
                        ));
                    } else if (type.equals(RequestActionHandler.NEW_THING)) {
                        Thing thing = (Thing) params[0];

                        int x = (int) params[1];
                        int y = (int) params[2];
                        agent.addEvent(new ThingEvent(
                            ThingEvent.ADDED,
                            Primitive.newPrimitive(thing.type),
                            Primitive.newPrimitive(thing.details),
                            Primitive.newPrimitive(x),
                            Primitive.newPrimitive(y)
                        ));
                    } else if (type.equals(RequestActionHandler.LAST_ACTION)) {
                        ListTerm args = new ListTerm();
                        args.add(Primitive.newPrimitive(params[0]));
                        args.add(asList((JsonNode) params[1]));
                        args.add(Primitive.newPrimitive(params[2]));
                        agent.addEvent(new SimEvent(Primitive.newPrimitive(type), args));
                    } else if (type.equals(RequestActionHandler.REQUEST_ACTION)) {
                        ListTerm args = new ListTerm();
                        args.add(Primitive.newPrimitive(params[0]));
                        args.add(Primitive.newPrimitive(params[1]));
                        gui.updateDisplay(model.toString(), model.actionLog);
                        agent.addEvent(new SimEvent(Primitive.newPrimitive(type), args));
                    }
                });

                // Parse the initial settings...
                JsonNode percept = message.get("content").get("percept");
                model.teamSize = percept.get("teamSize").asInt();
                model.team = percept.get("team").asText();
                model.name = percept.get("name").asText();
                model.vision = percept.get("vision").asInt();
                model.steps = percept.get("steps").asInt();

                ListTerm args = new ListTerm();
                args.add(Primitive.newPrimitive(model.team));
                args.add(Primitive.newPrimitive(model.steps));
                agent.addEvent(new SimEvent(Primitive.newPrimitive(MassimClient.SIM_START), args));

                // Register a new request-action handler with the new model attached to the client...
                client.registerHandler(MassimClient.REQUEST_ACTION, new RequestActionHandler(model));
        });
    }

    @FORMULA
    public Formula isFree(int x, int y) {
        return model.getMap().isFree(x, y) ? Predicate.TRUE:Predicate.FALSE;
    }

    @ACTION
    public boolean closestFree(int x, int y, int distance, ActionParam<Integer> tx, ActionParam<Integer> ty) {
        ClosestFree closestFree = new ClosestFree(model, x, y, distance);
        int[] result = closestFree.find();
        if (result == null) return false;
        tx.set(result[0]);
        ty.set(result[1]);
        return true;
    }

    @Override
    public void setAgent(Agent agent) {
        super.setAgent(agent);
        gui = new JPlayerGui(agent.name());
        EventQueue.invokeLater(() -> gui.display());
    }

    @TERM
    public Model model() {
        return model;
    }

    @FORMULA
    public Formula movedTeammates(int number) {
        return model.movedTeammates.size() == number ? Predicate.TRUE:Predicate.FALSE;
    }

    @TERM
    public int movedTeammates() {
        return model.movedTeammates.size();
    }

    @TERM
    public int unknowns() {
        List<Thing> things = model.getThings(Model.ENTITY, model.team);
        for (Player player : model.players.values()) {
             int index = indexOfPlayer(things, player);
             if (index > -1) {
                 things.remove(index);
             }
        }
        return things.size();
    }

    private int indexOfPlayer(List<Thing> things, Player player) {
        int i = 0;
        while (i < things.size() && matches(player, things.get(i))) i++;
        
        return i == things.size() ? -1:i;
    }

    private boolean matches(Player player, Thing thing) {
        return player.x == thing.x && player.y == thing.y;
    }

    @ACTION
    public boolean registerPlayer(String name, int tx, int ty) {
        Thing thing = model.movedTeammates.get(0);
        
        Player player = new Player(name, thing.x, thing.y, tx, ty);
        model.players.put(name, player);
        // System.out.println("[" + agent.name() + "] has registered: " + player);
        return true;
    }

    @ACTION
    public boolean registerOtherPlayer(String name, int tx, int ty) {
        Player player = new Player(name, model.x+tx, model.y+ty, tx, ty);
        model.players.put(name, player);
        // System.out.println("[" + agent.name() + "] has registered other: " + player);
        return true;
    }

    @ACTION
    public boolean movePlayer(String name, String dir) {
        if (!model.players.containsKey(name))
            throw new RuntimeException("Player is not registered as a friend");

        if (!dir.equals("-")) {
            model.players.get(name).move(dir);
            System.out.println("[" + agent.name() + "] has recorded: " + name + " moving: " + dir);
        }
        return true;
    }

    @TERM
    public ListTerm routeTo(int x, int y) {
        if (!model.inBounds(x, y)) throw new RuntimeException("Route target (" + x + "," + y +") is outside known area of agent");

        GradientMap map = new GradientMap(model);
        map.generateGradient(x, y);
        // System.out.println(map.toString());

        ListTerm list = new ListTerm();
        int[] coords = new int[] { model.x, model.y };
        while (map.getDistance(coords) > 1) {
            int option = map.chooseOption(coords);

            list.add(new Funct("location", new Term[] {
                Primitive.newPrimitive(coords[0]),
                Primitive.newPrimitive(coords[1])
            }));
            list.add(new Funct("move", new Term[] {
                Primitive.newPrimitive(map.optionDirection(option))
            }));
            coords = map.optionCoords(coords, option);
        }

        // System.out.println("list: " +list);
        return list;
    }

    @FORMULA
    public Formula isPlayer(String name) {
        return model.players.containsKey(name) ? Predicate.TRUE:Predicate.FALSE;
    }

    @ACTION
    public boolean connect(String host, int port) {
        return client.connect(host, port);
    }

    @ACTION
    public boolean start() {
        client.start();
        return true;
    }

    @ACTION
    public boolean authenticate(String user, String pass) {
        return client.authenticate(user, pass);
    }

    @ACTION
    @SuppressWarnings("unchecked")
    public boolean act(int id, String name, ListTerm params) {
        Object[] p = new Object[params.size()];
        for (int i=0; i<p.length; i++) {
            p[i] = ((Primitive<Object>) params.get(i)).value();
        }

        //Store action in model for review after step is performed...
        model.lastAction = new Action(id, name, p);
        client.act(model.lastAction);

        return true;
    }

    @ACTION
    public boolean location(ActionParam<Integer> x, ActionParam<Integer> y) {
        x.set(model.x);
        y.set(model.y);
        return true;
    }

    @FORMULA
    public Formula atLocation(int x, int y) {
        return model.x == x && model.y == y ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula step(int step) {
        return model.step == step ? Predicate.TRUE:Predicate.FALSE;
    }    

    @EVENT(symbols = {}, types = {"string", "list"}, signature = "$massim")
    public Event simEvent(Term type, Term params) {
        return new SimEvent(type, params);
    }

    @EVENT(symbols = {"+","-"}, types = {"string", "string", "int", "int"}, signature = "$mte")
    public Event thing(String modifier, Term type, Term details, Term x, Term y) {
        return new ThingEvent(Primitive.newPrimitive(modifier), type, details, x, y);
    }
        
    @TERM
    public ListTerm availableMoves() {
        ListTerm moves = new ListTerm();
        if (model.getMap().isFree(model.x, model.y-1)) {
            moves.add(NORTH);
        }
        if (model.getMap().isFree(model.x, model.y+1)) {
            moves.add(SOUTH);
        }
        if (model.getMap().isFree(model.x+1, model.y)) {
            moves.add(EAST);
        }
        if (model.getMap().isFree(model.x-1, model.y)) {
            moves.add(WEST);
        }
        return moves;
    }

    @TERM
    @SuppressWarnings("unchecked")
    public String randomMove() {
        ListTerm moves = availableMoves();
        if (moves.isEmpty()) return "none";
        return ((Primitive<String>) moves.get(random.nextInt(moves.size()))).value();
    }   
}