import java.util.ArrayList;
import java.util.List;

import astra.core.Module;
import astra.formula.Formula;
import astra.formula.Predicate;

public class Queue extends Module {
    List<String> list = new ArrayList<>();
    
    @ACTION
    public boolean enqueue(String name) {
        list.add(name);
        return true;
    }
    
    @TERM
    public String peek() {
        return list.get(0);
    }
    
    @ACTION
    public boolean dequeue() {
        list.remove(0);
        return true;
    }
    
    @FORMULA
    public Formula isEmpty() {
        return list.isEmpty() ? Predicate.TRUE:Predicate.FALSE;
    }

    @FORMULA
    public Formula isFront(String name) {
        return !list.isEmpty() && list.get(0).equals(name) ? Predicate.TRUE:Predicate.FALSE;
    }
}