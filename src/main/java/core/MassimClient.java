package core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import assemble.Model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import core.Action;

public class MassimClient {
    public static final String SIM_START = "sim-start";
    public static final String REQUEST_ACTION = "request-action";
    public static final String ACTION = "action";
    public static final String OK = "ok";
    public static final String AUTH_REQUEST = "auth-request";
    public static final String TYPE = "type";
    public static final String BYE = "bye";

    public static final ObjectMapper mapper = new ObjectMapper();
    private Map<String, MessageHandler> handlers = new HashMap<>();
    private Socket socket;
    public String agent;    // barry: testing

    /**
     * ---------------START--------------
     * barry's testing code 
     */

    Model model;
    public MassimClient(Model model){
        this.model = model;
    }

    /**
     * barry's testing code 
     * ----------------END----------------
     */

    public void registerHandler(String type, MessageHandler handler) {
        handlers.put(type, handler);
    }

    public boolean connect(String host, int port) {
        try {
            socket = new Socket(host, port);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

   public void start() {
        new Thread(() -> {
            JsonNode node = getJsonTreeResponse();
            String type = node.get(TYPE).asText();
            while (!type.equals(BYE)) {
                MessageHandler handler = handlers.get(type);
                if (handler != null) {
                    handler.handle(node);
                } else {
                    System.out.println("Message Type Not Handled: " + type);
                }

                /**
                 * START 
                 * Test Action
                 */
        
                if (type.equals("request-action")){
                    JsonNode content = node.get("content");
                    int stepID = content.get("id").asInt();
                    System.out.println("STEP ID: "+stepID);

                    String actionString = "";
                    Object [] objects = new Object[5];
                    String s = "";

                    Scanner scan = new Scanner(System.in);
                    System.out.println("Enter next move "+agent+": ");
                    s= scan.next();

                    // stepID = scan.nextInt();
                
                    if(s.charAt(0) == 'm') actionString = "move";
                    if(s.charAt(0) == 'a') actionString = "attach";
                    if(s.charAt(0) == 'd') actionString = "detach";
                    if(s.charAt(0) == 'r') actionString = "request";
                    if(s.charAt(0) == 'o') actionString = "rotate";
                    if(s.charAt(0) == 'c') actionString = "connect";
                    if(s.charAt(0) == 'i') actionString = "disconnect";
                    if(s.charAt(0) == 'l') actionString = "clear";
                    objects[0] = s.substring(1,2);

                    if(actionString.equals("rotate")){
                        objects[0] = s.substring(1,4);
                    }
                    
                    if (s.charAt(0) == 'c' || s.charAt(0) == 'i'){

                        if(agent.equals("agentA11")){
                            objects[0] = "agentA12";
                        }
                        else if(agent.equals("agentA12")){
                            objects[0] = "agentA11";
                        }

                        if(s.charAt(1) == '-' && s.charAt(3) != '-'){
                            objects[1] = "-" + String.valueOf(s.charAt(2));
                            objects[2] = s.charAt(3);        
                        }
                        else if(!(s.charAt(1) == '-') && !(s.charAt(2) == '-')){
                            objects[1] = String.valueOf(s.charAt(1));
                            objects[2] = String.valueOf(s.charAt(2));
                        }
                        else if(!(s.charAt(1) == '-') && (s.charAt(2) == '-')){
                            objects[1] = String.valueOf(s.charAt(1));
                            objects[2] = String.valueOf(s.charAt(2)) + String.valueOf(s.charAt(3)); 
                        }
                        else if((s.charAt(1) == '-') && (s.charAt(2) == '-')){
                            objects[1] = String.valueOf(s.charAt(1)) + String.valueOf(s.charAt(2)); 
                            objects[2] = String.valueOf(s.charAt(3)) + String.valueOf(s.charAt(4)); 
                        }
                        
                      
                        
                    }
                    if (s.charAt(0) == 'i'){

                        // objects[0] = s.substring(1, 2);
                        // objects[1] = s.substring(2, 3);
                        // objects[2] = s.substring(3, 4);
                        // objects[3] = s.substring(4, 5);
                        if(s.charAt(3) == '-' && s.charAt(5) != '-'){
                            objects[3] = "-" + String.valueOf(s.charAt(4));
                            objects[4] = s.charAt(5);        
                        }
                        else if(!(s.charAt(3) == '-') && !(s.charAt(4) == '-')){
                            objects[3] = String.valueOf(s.charAt(3));
                            objects[4] = String.valueOf(s.charAt(4));
                        }
                        else if(!(s.charAt(3) == '-') && (s.charAt(4) == '-')){
                            objects[3] = String.valueOf(s.charAt(3));
                            objects[4] = String.valueOf(s.charAt(4)) + String.valueOf(s.charAt(5)); 
                        }
                        else if((s.charAt(3) == '-') && (s.charAt(4) == '-')){
                            objects[3] = String.valueOf(s.charAt(3)) + String.valueOf(s.charAt(4)); 
                            objects[4] = String.valueOf(s.charAt(5)) + String.valueOf(s.charAt(6)); 
                        }    
                    }
                    else if (s.charAt(0) == 'l'){
                        objects[0] = s.substring(1, 2);
                        objects[1] = s.substring(2, 3);
                    }
                Action action = new Action(stepID, actionString, objects);
                act(action);
                model.lastAction = action;
                    
                }
               
                /**
                 * Test Action 
                 * END
                 */

                node = getJsonTreeResponse();
                type = node.get(TYPE).asText();
            }
        }).start();
    }

    public boolean authenticate(String user, String pass) {
        agent = user; // barry: testing
        AuthRequestMessage message = new AuthRequestMessage(AUTH_REQUEST, new Credentials(user, pass));
        try {
            socket.getOutputStream().write(mapper.writeValueAsString(message).getBytes(StandardCharsets.UTF_8));
            socket.getOutputStream().write(new byte[]{0});
            return getAuthResponse().content.result.equals(OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean act(Action action) {
        ActionMessage message = new ActionMessage(ACTION, action);

        try {
            socket.getOutputStream().write(mapper.writeValueAsString(message).getBytes(StandardCharsets.UTF_8));
            socket.getOutputStream().write(new byte[]{0});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }    
    
    private ByteArrayOutputStream getRawResponse() {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int b = -1;
        try {
            while (true) {
                b = socket.getInputStream().read();
                if (b == 0) break; // message completely read
                else if (b == -1) return null; // stream ended
                else buffer.write(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buffer;
    }
    
    private JsonNode getJsonTreeResponse() {
        try {
            return mapper.readTree(getRawResponse().toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private AuthResponseMessage getAuthResponse() {
        try {
            return mapper.readValue(getRawResponse().toByteArray(), AuthResponseMessage.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }    
}