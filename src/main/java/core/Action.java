package core;

public class Action {
    public int id;
    public String type;
    public Object[] p;

    public Action(int id, String type, Object[] p) {
        this.id = id;
        this.type = type;
        this.p = p;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(type).append("(");
        for (int i=0; i<p.length;i++) {
            if (i>0) builder.append(", ");
            builder.append(p[i]);
        }
        builder.append(")");
        return builder.toString();
    }

    public boolean equals(Object object) {
        if (!Action.class.isInstance(object)) return false;
        Action action = (Action) object;
        if (!type.equals(action.type)) return false;
        for (int i=0; i<p.length;i++) {
            if (!p[i].equals(action.p[i])) return false;
        }
        return true;
    }
}