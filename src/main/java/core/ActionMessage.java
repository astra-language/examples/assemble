package core;

public class ActionMessage extends Message {
    public ActionMessage(String type, Action content) {
        super(type);
        this.content = content;
    }

    public Action content;
}