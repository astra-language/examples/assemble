package core;

public class AuthRequestMessage extends Message {
    public AuthRequestMessage(String type, Credentials content) {
        super(type);
        this.content = content;
    }

    public Credentials content;
}