package event;

import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Primitive;
import astra.term.Term;

public class ThingEvent implements Event {
    public static final Primitive<String> ADDED = Primitive.newPrimitive("+");
    public static final Primitive<String> REMOVED = Primitive.newPrimitive("-");
    
    private Term modifier;
    private Term type;
    private Term details;
    private Term x;
    private Term y;

    public ThingEvent(Term modifier, Term type, Term details, Term x, Term y) {
        this.modifier = modifier;
        this.type = type;
        this.details = details;
        this.x = x;
        this.y = y;
    }

    public Term modifier() {
        return modifier;
    }

    public Term type() {
        return type;
    }

    public Term details() {
        return details;
    }

    public Term x() {
        return x;
    }

    public Term y() {
        return y;
    }

    @Override
    public Event accept(LogicVisitor visitor) {
        return new ThingEvent(modifier, 
            (Term) visitor.visit(type),
            (Term) visitor.visit(details),
            (Term) visitor.visit(x),
            (Term) visitor.visit(y)
        );
    }

    @Override
    public Object getSource() {
        return null;
    }

    @Override
    public String signature() {
        return "$mte";
    }

}