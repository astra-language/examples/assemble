package event;

import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class SimEventUnifier implements EventUnifier<SimEvent> {
	public Map<Integer, Term> unify(SimEvent source, SimEvent target, Agent agent) {
            return Unifier.unify(
                  new Term[] {source.type(), source.params()},
                  new Term[] {target.type(), target.params()},
                  new HashMap<>(), agent);
	}
}