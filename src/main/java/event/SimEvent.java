package event;

import astra.event.Event;
import astra.reasoner.util.LogicVisitor;
import astra.term.Term;

public class SimEvent implements Event {
    private Term type;
    private Term params;

    public SimEvent(Term type, Term params) {
        this.type = type;
        this.params = params;
    }

    public Term type() {
        return type;
    }

    public Term params() {
        return params;
    }

    @Override
    public Event accept(LogicVisitor visitor) {
        return new SimEvent((Term) visitor.visit(type), (Term) visitor.visit(params));
    }

    @Override
    public Object getSource() {
        return null;
    }

    @Override
    public String signature() {
        return "$massim";
    }

    @Override
    public String toString() {
        return "simEvent(" + type + "," + params + ")";
    }

}