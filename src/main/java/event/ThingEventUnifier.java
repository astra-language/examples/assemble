package event;

import java.util.HashMap;
import java.util.Map;

import astra.core.Agent;
import astra.reasoner.EventUnifier;
import astra.reasoner.Unifier;
import astra.term.Term;

public class ThingEventUnifier implements EventUnifier<ThingEvent> {
	public Map<Integer, Term> unify(ThingEvent source, ThingEvent target, Agent agent) {
		return Unifier.unify(
            new Term[] {source.modifier(), source.type(), source.details(), source.x(), source.y()},
            new Term[] {target.modifier(), target.type(), target.details(), target.x(), target.y()},
            new HashMap<Integer, Term>(), agent);
	}
}