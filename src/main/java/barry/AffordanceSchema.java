package barry;

import assemble.Model;

public interface AffordanceSchema {
    void checkAffordance(Model model, int x, int y);
}
