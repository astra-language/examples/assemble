package barry;

import assemble.Model;
import core.Action;

public class MoveEastSchema implements AffordanceSchema {

    @Override
    public void checkAffordance(Model model, int x, int y) {
        if (model.getMap().isFree(x+1, y)) {
            model.affordances.add(new Action(-1, "move", new Object[] {"e"}));
        } else {
            System.out.println("EAST:");
            System.out.println("Terrain: " + model.getMap().getTerrain(x+1, y));
            System.out.println("Things: " + model.getMap().getThings(x+1, y));
        }
    }
}
