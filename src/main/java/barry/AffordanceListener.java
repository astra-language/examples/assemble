package barry;

import java.util.LinkedList;
import java.util.List;

import assemble.Model;
import assemble.ModelListener;

public class AffordanceListener implements ModelListener {
    static List<AffordanceSchema> schemas = new LinkedList<>();

    static {
        schemas.add(new MoveNorthSchema());
        schemas.add(new MoveSouthSchema());
        schemas.add(new MoveEastSchema());
        schemas.add(new MoveWestSchema());
    }

    private Model model;
    
    public AffordanceListener(Model model) {
        this.model = model;
    }
    @Override
    public void receive(String type, Object[] params) {
        if (type.equals("request-action")) {
            model.affordances.clear();

            int x = model.x;
            int y = model.y;

            System.out.println("Checking: " + x +","+y);
            // System.out.println(model.toString());

            for (AffordanceSchema schema : schemas) {
                schema.checkAffordance(model, x, y);
            }
            
            model.notify("reason", params);
        }
    }
    
}
