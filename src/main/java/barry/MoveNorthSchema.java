package barry;

import assemble.Model;
import core.Action;

public class MoveNorthSchema implements AffordanceSchema {

    @Override
    public void checkAffordance(Model model, int x, int y) {
        if (model.getMap().isFree(x, y-1)) {
            model.affordances.add(new Action(-1, "move", new Object[] {"n"}));
        } else {
            System.out.println("NORTH:");
            System.out.println("Terrain: " + model.getMap().getTerrain(x, y-1));
            System.out.println("Things: " + model.getMap().getThings(x, y-1));
        }
    }
    
}
