package barry;

import assemble.Model;
import core.Action;

public interface Strategy {
    Action selectAction(Model model);
}
