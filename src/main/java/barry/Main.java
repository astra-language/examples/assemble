package barry;

import assemble.Model;
import assemble.ModelListener;
import assemble.RequestActionHandler;
import assemble.SimStartHandler;
import core.MassimClient;
import core.Action;


public class Main {

    
    private Model model = new Model();
    private MassimClient client = new MassimClient(model);
    // private ActionModel actionModel = new ActionModel(model);
    
    public static void main(String[] args) {
        new Main().start("agentA11","1");
        // new Main().start("agentA12","1");
    }

    public void start(String agent, String password) {
        model.register(new ModelListener(){
            @Override
            public void receive(String type, Object[] params) {
                // System.out.println("Received Event: " + type);
            }
        });
        client.registerHandler(MassimClient.SIM_START, new SimStartHandler(model));
        client.registerHandler(MassimClient.REQUEST_ACTION, new RequestActionHandler(model));

        // client.addActionModel(actionModel);

        client.connect("localhost", 12300);
        client.authenticate(agent, password);
        client.start();
    }
    
}





// package barry;

// import java.util.LinkedList;
// import java.util.List;
// import java.util.Map;

// import assemble.ClosestFree;
// import assemble.ExploreGrid;
// import assemble.GradientMap;
// import assemble.Model;
// import assemble.ModelListener;
// import assemble.RequestActionHandler;
// import assemble.SimStartHandler;
// import assemble.Task;
// import assemble.Thing;
// import core.MassimClient;
// import core.Action;


// public class Main {

    
//     private Model model = new Model();
//     List<String> route = null;
//     String state = "explore";
//     int [][] searchPattern = new int[][]{new int[]{0,5}, new int[]{0,5}, new int[]{0,5}, new int[]{-5,0}, new int[]{-5,0}};
//     int searchStep = 0;
//     ExploreGrid exploreGrid;
//     private MassimClient client = new MassimClient(model);  // testing purposes
//     // private ActionModel actionModel = new ActionModel(model);
    
//     public static void main(String[] args) {
//         // new Main().start("agentA11","1");
//         new Main().start("agentA12","1");
//         // new Main().start("agentB10","1");
//     }

//     public void start(String agent, String password) {
//         model.register(new AffordanceListener(model));
//         // model.register(new rem.ReasonerListener(client, model));
//         model.register(new ModelListener(){
//             @Override
//             public void receive(String type, Object[] params) {
//                 System.out.println("Received Event: " + type);
//                 if(type.equals(RequestActionHandler.REQUEST_ACTION)){
//                     // check to see if there is a task achievable

//                     if((model.getThings("taskboards","").size() > 0) && !(state.equals("performTask"))){
//                         int minReward = 10000;
//                         Task task = new Task();
//                         for (Map.Entry<String, Task> entry : model.taskMap.entrySet()){
//                             if(entry.getValue().reward < minReward){
//                                 minReward = entry.getValue().reward;
//                                 task = entry.getValue();
//                             }
//                         }
//                         model.task = task;
//                         state = "performTask";
//                     }

//                     if (state.equals("performTask")){
//                         System.out.println("Agent is now performing a task");
                        
//                         // First, find a taskboard so that the agent can accept a task
//                         List<Thing> taskboards = model.getThings("taskboard", "");
//                         System.out.println(taskboards.size());
//                         Thing taskboard = taskboards.get(0);    // Choose first taskboard in list
//                         route = planRoute(taskboard.x, taskboard.y);
//                         int stepID = (Integer) params[0];
//                         String direction = route.remove(0);
//                         Action action = new Action(stepID, "move", new Object[]{direction});
//                         client.act(action);
//                         model.lastAction = action;
//                         if(model.computeManhattanDistance(2)){
//                             System.out.println("Agent can now accept task!");
//                         }
//                         // String block = model.task.requirements.get(0).type;  // Take first block in list 
//                         // Thing dispenser;
//                         // if (!(model.getThings("dispenser",block) == null)){
//                         //     List<Thing> dispensers = model.getThings("dispenser",block);
//                         //     dispenser = dispensers.get(0);    // Take first dispenser in list
//                         // }
//                         // else if (model.getThings("dispenser",block) == null){
//                         //     state = "findDispenser";
//                         // }
//                     }

//                     if(state.equals("explore")){
//                         // Check for end of explore
                        
//                         if(route == null){
//                             int[] offset = searchPattern[searchStep++];
//                             int [] coords = new int[]{model.x+offset[0],model.y+offset[1]};
//                             if(!model.getMap().isFree(coords[0],coords[1])){
//                                 System.out.println("obstacle "+coords[0]+":"+coords[1]);
//                                 ClosestFree cf = new ClosestFree(model,coords[0],coords[1],3);
//                                 coords = cf.find();
//                             }
//                             // only happens first time
//                             route = planRoute(coords[0],coords[1]);
//                         }
//                         else if(route.isEmpty()){
//                             if(searchStep == searchPattern.length){
//                                 searchStep = 0;
//                             }
//                             int[] offset = searchPattern[searchStep++];
//                             int [] coords = new int[]{model.x+offset[0],model.y+offset[1]};
//                             if(!model.getMap().isFree(coords[0],coords[1])){
//                                 System.out.println("obstacle");
//                                 ClosestFree cf = new ClosestFree(model,coords[0],coords[1],3);
//                                 coords = cf.find();
//                             }
//                             // only happens first time
//                             route = planRoute(coords[0],coords[1]);
//                         }
//                         int stepID = (Integer) params[0];
//                         String direction = route.remove(0);
//                         Action action = new Action(stepID, "move", new Object[]{direction});
//                         client.act(action);
//                         model.lastAction = action;
//                     }
//                     else if(state.equals("exploreGrid")){
//                         if(exploreGrid == null){
//                             exploreGrid = new ExploreGrid(model.x, model.y);
//                             exploreGrid.addOptions(model.x, model.y);
//                         }
//                         if(route == null){
//                             int[] coords = exploreGrid.selectTarget();
//                             route = planRoute(coords[0],coords[1]);
//                         }
//                         else if(route.isEmpty()){
//                             exploreGrid.markVisited(model.x, model.y);
//                             exploreGrid.addOptions(model.x, model.y);
//                             int[] coords = exploreGrid.selectTarget();
//                             route = planRoute(coords[0],coords[1]);
//                         }
//                         int stepID = (Integer) params[0];
//                         String direction = route.remove(0);
//                         Action action = new Action(stepID, "move", new Object[]{direction});
//                         client.act(action);
//                         model.lastAction = action;
                    
                    
                        
//                     }
//                 }

//             }
//         });
//         client.registerHandler(MassimClient.SIM_START, new SimStartHandler(model));
//         client.registerHandler(MassimClient.REQUEST_ACTION, new RequestActionHandler(model));

//         // client.addActionModel(actionModel);

//         client.connect("localhost", 12300);
//         client.authenticate(agent, password);
//         client.start();
//     }
//     public List<String> planRoute(int x, int y){
//         List<String> directions = new LinkedList<>();
//         GradientMap map = new GradientMap(model);
//         map.generateGradient(x, y);
//         // System.out.println(map.toString());

//         int[] coords = new int[] { model.x, model.y };
//         while (map.getDistance(coords) > 1) {
//             int option = map.chooseOption(coords);
//             directions.add(map.optionDirection(option));
//             coords = map.optionCoords(coords, option);
//         }
//         return directions;
//     }
    
// }
