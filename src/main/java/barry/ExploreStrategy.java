package barry;

import assemble.Model;
import core.Action;

public class ExploreStrategy implements Strategy {
    private int direction = 0;

    private static Action[] EXPLORE_ACTIONS = new Action[] {
        new Action(-1, "move", new Object[]{"n"}), 
        new Action(-1, "move", new Object[]{"e"}),
        new Action(-1, "move", new Object[]{"s"}),
        new Action(-1, "move", new Object[]{"w"})
    }; 

    @Override
    public Action selectAction(Model model) {
        int initial = direction;
        while (!model.affordances.contains(EXPLORE_ACTIONS[direction])) {
            direction = direction + 1 % EXPLORE_ACTIONS.length;
            if (initial == direction) {
                System.out.println("Blocked!");
                break;
            }
        }

        return EXPLORE_ACTIONS[direction];
    }
}
