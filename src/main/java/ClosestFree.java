import java.util.Map;
import java.util.TreeMap;

import assemble.Model;
import graph.util.Heap;
import graph.util.Position;

public class ClosestFree {
    private static final int[][] OPTIONS = {new int[] {0, -1}, new int[] {1, 0}, new int[] {0,1}, new int[] {-1,0}};

    public class Entry {
        int x;
        int y;
        int distance;

        public Entry(int x, int y, int distance) {
            this.x = x;
            this.y = y;
            this.distance = distance;
        }

        public String toString() {
            return x + "," +y;
        }
    }

    private Model model;
    private int hx;
    private int hy;
    private int range;

	public ClosestFree(Model model, int x, int y, int range) {
        this.model = model;
        this.hx = x;
        this.hy = y;
        this.range = range;
	}
    
    public int[] find() {
        Heap<Integer, Entry> heap = new Heap<>();
        Map<String, Position<Entry>> positionMap = new TreeMap<>();
        
        Entry entry = new Entry(hx, hy, 0);
        String key = key(hx, hy);
        positionMap.put(key, heap.insert(entry.distance, entry));
        while (!heap.isEmpty()) {
            entry = heap.remove();

            if (model.getMap().isFree(entry.x, entry.y)) {
                return new int[] {entry.x, entry.y};
            } else {
                for (int[] coord : OPTIONS) {
                    int cx = entry.x+coord[0];
                    int cy = entry.y+coord[1];
    
                    if (inBounds(cx, cy)) {
                        String k = key(cx, cy);
                        if (positionMap.containsKey(k)) {
                            Position<Entry> position = positionMap.get(k);
                            if (position.element().distance > entry.distance+1) {
                                //better path has been found...
                                position.element().distance = entry.distance+1;
                                heap.replaceKey(position, entry.distance+1);
                            }
                        } else {
                            positionMap.put(k, heap.insert(
                                entry.distance+1,
                                new Entry(cx, cy, entry.distance+1)
                            ));
                        }
                    }
                }
            }
        }
        return null;
    }

    private String key(int x, int y) {
        return x+":"+y;
    }

    private boolean inBounds(int x, int y) {
        return x >= hx-range && x <= hx+range && y >= hy-range && y <= hy+range;
    }
}