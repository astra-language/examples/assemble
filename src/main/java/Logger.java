import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.TreeMap;

import astra.core.Module;

public class Logger extends Module {
    public static final int OFF = 0;
    public static final int SEVERE = 1;
    public static final int ERROR = 2;
    public static final int WARN = 3;
    public static final int INFO = 4;

    static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");  
    static String[] label = {"OFF", "SEVERE", "ERROR", "WARN", "INFO"};
    static Map<String, Integer> levelMap = new TreeMap<>();

    static {
        levelMap.put("off", OFF);
        levelMap.put("severe", SEVERE);
        levelMap.put("error", ERROR);
        levelMap.put("warn", WARN);
        levelMap.put("info", INFO);
        levelMap.put("off", OFF);
        levelMap.put("SEVERE", SEVERE);
        levelMap.put("ERROR", ERROR);
        levelMap.put("WARN", WARN);
        levelMap.put("INFO", INFO);
        levelMap.put("OFF", OFF);
    }

    int level;

    private boolean log(int level, String source, String message) {
        if (level <= this.level) {
            System.out.println(dtf.format(LocalDateTime.now()) + " ["+source+"] " + label[level] + ": " + message);
        }
        return true;
    }

    @ACTION
    public boolean setLevel(String level) {
        this.level = levelMap.get(level);
        return true;
    }

    @ACTION
    public boolean log(int level, String message) {
        return log(level, agent.name(), message);
    }
    @ACTION
    public boolean severe(String message) {
        return log(SEVERE, agent.name(), message);
    }

    @ACTION
    public boolean error(String message) {
        return log(ERROR, agent.name(), message);
    }

    @ACTION
    public boolean warn(String message) {
        return log(WARN, agent.name(), message);
    }

    @ACTION
    public boolean info(String message) {
        return log(INFO, agent.name(), message);
    }
}