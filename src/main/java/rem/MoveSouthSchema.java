package rem;

import assemble.Model;
import core.Action;

public class MoveSouthSchema implements AffordanceSchema {

    @Override
    public void checkAffordance(Model model, int x, int y) {
        if (model.getMap().isFree(x, y+1)) {
            model.affordances.add(new Action(-1, "move", new Object[] {"s"}));
        } else {
            System.out.println("SOUTH:");
            System.out.println("Terrain: " + model.getMap().getTerrain(x, y+1));
            System.out.println("Things: " + model.getMap().getThings(x, y+1));
        }
    }
    
}
