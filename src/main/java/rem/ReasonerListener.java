package rem;

import assemble.Model;
import assemble.ModelListener;
import core.Action;
import core.MassimClient;

public class ReasonerListener implements ModelListener {
    private Model model;
    private MassimClient client;
    private Strategy strategy;

    
    public ReasonerListener(MassimClient client, Model model) {
        this.client = client;
        this.model = model;
        strategy = new ExploreStrategy();
    }
    
    @Override
    public void receive(String type, Object[] params) {
        if (type.equals("reason")) {
            Action action = strategy.selectAction(model);
            action.id = (Integer) params[0];
            System.out.println("Acting: " + action);
            model.lastAction = action;
            client.act(action);
        }

    }
}
